# SDCE取币（转币）流程及API说明
    api url:
    生产环境： https://api.sdce.com.au
    开发环境： https://dev-api.sdce.com.au
    
    auth0 config params:
    生产环境：
      domain: 'sdce.au.auth0.com',
      clientID: 'juxW8MuSfS4AxEA1DgJLO1i7HQsoVBgl',
    开发环境：
      domain: 'sdce.au.auth0.com',
      clientID: 'PPdR7LiL0o1ASMAWOqHD7GeiOzzKoD65',
---

## 取币流程
* 注册并创建用户
* 通过KYC个人身份验证(此步骤会要求验证邮箱和绑定手机)
* 获取货币信息列表和用户账户信息
* 选择货币，并添加取币/转币账户（最终到账账户）
* 发送邮件和短信验证码
* 选择已添加的取币地址，连同数额及验证码提交
* 等待管理员审核（若转入转出账户同为SDCE内部账户，则不需要管理员审核，并且不收取手续费）
* 所有取款和存款（转入转出）记录可以在账户记录中查到


## API 说明
* 用户信息
  * [获取 accessToken](#获取accessToken)
  * [获取用户信息](#获取用户信息)
* 钱包列表
  * [获取所有货币信息](#获取所有货币信息)
  * [获取用户账户信息](#获取用户账户信息)
* 取币
  * [创建数字货币取款地址](#创建数字货币取款地址)
  * [获取用户所有数字货币取款地址](#获取用户所有数字货币取款地址)
  * [更改默认数字货币取款地址](#更改默认数字货币取款地址)
  * [删除数字货币取款地址](#删除数字货币取款地址)
  * [取币手续费与最小限额](#取币手续费与最小限额)
  * [提交取币请求](#提交取币请求)
  * [发送邮件验证码](#发送邮件验证码)
  * [发送短信验证码](#发送短信验证码)
* 存币
  * 数字货币地址可在账户信息中找到 [获取用户账户信息](#获取用户账户信息)
* 账户记录
  * [存入记录](#存入记录)
  * [取出记录](#取出记录)

---

## API 说明

### 获取accessToken
> 该方法仅为现阶段测试API时使用
1. 前往https://dev-exchange.sdce.com.au/ 注册账号（测试环境）
2. 前往 `账户与安全` 验证邮箱和绑定手机号
3. telegram联系sdce手动通过身份验证，用于进一步测试
4. 可通过浏览器console中的request查看Authorization字段中的token，或使用`localStorage.accessToken`获取accessToken

---

### 获取用户信息
`GET` /exchange/member
* 头部信息
  * Content-Type: application/json
  * Authorization: Bearer {accessToken}

返回数据: 用户的相关信息，**其中id字段为memberId**

Sample response:
```json
{
  "authid": "auth0|5c98250953fb160ec8e9b96f",
  "contact": {
    "email": "xxx+super@sdce.com.au",
    "phoneNumber": "+610420390409",
    "phoneVerified": true
  },
  "createdAt": "2019-03-25T00:47:11.681Z",
  "director": {
    "declarePhoto": "/kyc/a1980c59-193f-4140-9caf-cf123fa0a1de_u=984943859,1381222648&fm=11&gp=0.jpg",
    "registrantPhotoIdBack": "/kyc/5998302d-98be-490c-8524-b3d42d5d57a9_haha.png",
    "registrantPhotoIdFront": "/kyc/5f047783-a4b9-4580-84ec-c878fd1154cc_haha-no-mouth.png"
  },
  "id": "5c98250fe60da8cb0eb5cba3",
  "inviteCode": "1933075630",
  "kycLevel": "KYC_IDENTITY_VERIFIED",
  "memberLevel": {
    "coinDepositLimit": "0",
    "coinWithdrawLimit": "0",
    "depositAudLimit": "0",
    "level": 0,
    "withdrawAudLimit": "0"
  },
  "otcDetail": {
    "alipayDetail": {
      "QRCode": "/otc/c265e19e-51f2-4db3-b6ef-ad1ca2b9367a_0po3o52qnps0470onp3nrq57oqrso84q.jpg",
      "accountNumber": "支付宝85742396",
      "name": "庄晓曼"
    },
    "bankPaymentDetail": {
      "accountNumber": "6230580000083608078",
      "bankBranchInfo": "学院路支行",
      "firstName": "途",
      "lastName": "肖"
    },
    "nickName": "庄晓曼",
    "wechatPaymentDetail": {
      "QRCode": "/otc/e9cacd37-9429-49a8-ac66-8390bde4d2da_wechatpay.png",
      "accountNumber": "晓曼的微信"
    }
  },
  "otcFeeRate": "0.002",
  "portfolio": {
    "assets": [
      {
        "balance": "5704319",
        "currency": "BTC"
      },
      {
        "balance": "990",
        "currency": "USDT"
      },
      {
        "balance": "197250",
        "currency": "ETH"
      }
    ],
    "audTxFeeRate": "0.0010",
    "tokenTxRate": "0.0022",
    "total30dTradeAudVal": "82997061270008"
  },
  "referralSummary": {
    "commissionRate": "0.20",
    "referrals": [
      {
        "email": "yulan.huang+test1@sdce.com.au",
        "id": "5cb7c298021f35c478cf9118",
        "joinedDate": "2019-04-18T00:19:36.684Z"
      },
      {
        "email": "xxx+ref3@sdce.com.au",
        "id": "5cb7c2a454065101ac35e9f3",
        "joinedDate": "2019-04-18T00:19:48.756Z"
      },
      {
        "email": "xxx+ref4@sdce.com.au",
        "id": "5cb805dcb9c04babef8e9587",
        "joinedDate": "2019-04-18T05:06:36.500Z"
      },
      {
        "email": "xxx+refer1@sdce.com.au",
        "id": "5cbe8891fa14697e6e3de4c4",
        "joinedDate": "2019-04-23T03:37:53.606Z"
      },
      {
        "email": "xxx+refer2@sdce.com.au",
        "id": "5cbe8a240d8b094ef8c5b08e",
        "joinedDate": "2019-04-23T03:44:36.816Z"
      }
    ]
  },
  "registrant": {
    "address": {
      "address1": "56, Pitt"
    },
    "dob": "1990-01-01",
    "firstName": "UI",
    "idNumber": "1234567890",
    "lastName": "Name"
  },
  "source": "International",
  "type": "Individual",
  "updatedAt": "2019-03-25T00:47:11.681Z",
  "walletUID": 120
}
```

---

### 获取所有货币信息
`GET` /exchange/currency
* 头部信息
  * Content-Type: application/json
  * Authorization: Bearer {accessToken}

返回数据: 所有的货币数据（法币 and 数字货币）
```json
[
  {
    "contractAddress": "addressUSD",
    "decimal": 2,
    "id": "5ce39af17cc228b2c3034ef5",
    "logo": "https://icons-exchange.oss-ap-southeast-2.aliyuncs.com/icon-usd.png",
    "name": {
      "en": "USD",
      "zh": "美元"
    },
    "status": "ACTIVE",
    "symbol": "usd",
    "type": "CURRENCY_FIAT"
  },
  {
    "contractAddress": "contractAddressETH",
    "decimal": 18,
    "id": "5c7c67dd21afc513a54c7781",
    "logo": "https://upload.wikimedia.org/wikipedia/commons/7/70/Ethereum_logo.svg",
    "name": {
      "en": "ETH",
      "zh": "ETH"
    },
    "status": "ACTIVE",
    "symbol": "eth",
    "type": "CURRENCY_ETH"
  },
  {
    "contractAddress": "contractAddressBTC",
    "decimal": 8,
    "id": "5c7c67dd21afc513a54c7782",
    "logo": "https://upload.wikimedia.org/wikipedia/commons/9/9a/BTC_Logo.svg",
    "name": {
      "en": "BTC",
      "zh": "BTC"
    },
    "status": "ACTIVE",
    "symbol": "btc",
    "type": "CURRENCY_BTC"
  },
  {
    "contractAddress": "contractAddressAUD",
    "decimal": 10,
    "id": "5c7c67de21afc513a54c7783",
    "logo": "https://cdn.sdce.com.au/images/icon-aud.png",
    "name": {
      "en": "AUD",
      "zh": "AUD"
    },
    "status": "ACTIVE",
    "symbol": "aud",
    "type": "CURRENCY_FIAT"
  },
  ...
]
```
---

### 获取用户账户信息
`GET` /exchange/member/{memberId}/account
* 头部信息
  * Content-Type: application/json
  * Authorization: Bearer {accessToken}
* 路径参数
  * memberId(必填): member ID
* 查询参数
  * pageIndex(必填): 第几页，从0开始
  * pageSize(必填): 每页数据条数
  * coinID(选填): 根据货币ID筛选
  * statusList(选填，暂时没有用到)

Sample request
```
https://dev-api.sdce.com.au/exchange/member/5c98250fe60da8cb0eb5cba3/account?pageIndex=0&pageSize=10
```

返回数据: 用户账户数据
```json
{
  "Result": {
    "account": [
      {
        "Status": "Primary",
        "balance": "1000",
        "bankDetails": [],
        "createdAt": "2019-03-27T01:42:12.285Z",
        "currency": {
          "decimal": 18,
          "id": "5c7c67dd21afc513a54c7781",
          "logo": "https://upload.wikimedia.org/wikipedia/commons/7/70/Ethereum_logo.svg",
          "name": {
            "en": "ETH",
            "zh": "ETH"
          },
          "symbol": "eth",
          "type": "CURRENCY_ETH"
        },
        "depositAddress": "0x92DCe16fA5f035eAd45c11B3A0dBa929B72A77AD",
        "id": "5c9ad4f480b8a4c2db15a944",
        "onHold": "0",
        "owner": "5c98250fe60da8cb0eb5cba3",
        "type": "Spot",
        "updatedAt": "2019-03-27T01:42:12.285Z",
        "walletDetails": [
          {
            "accountId": "5c9ad4f480b8a4c2db15a944",
            "address": "0x59749b0D34BaC5603B0E592cfdA665Da2c62dfea",
            "isDefault": true,
            "label": "Shawn",
            "walletId": "5c9c61e9b7aac39cce876516"
          }
        ]
      },
      ...
    ]
  }
}
```

---

### 存入记录
GET /exchange/member/{memberId}/deposits

查询参数：
- accountId: string // AUD account ID
- pageIndex： integer// 返回存款历史列表的页数的index
- pageSize: integer //每页包含多少存款历史

返回结果：

```
{
    "result": [
        {
            "accountId": "5cac769747e672679f87832e",
            "amount": "1000",
            "createdAt": "2019-06-05T06:10:27.656Z",
            "currency": {
                "decimal": 10,
                "id": "5c7c67de21afc513a54c7783",
                "logo": "https://cdn.sdce.com.au/images/icon-aud.png",
                "name": {
                    "en": "AUD",
                    "zh": "AUD"
                },
                "symbol": "aud",
                "type": "CURRENCY_FIAT"
            },
            "id": "5cf75cd3ae1d611a1addd4f7",
            "status": "DEPOSIT_STATUS_SUBMITTED",  //存款事件状态
            "type": "TRANSACTION_FIAT"
        },
        {
            "accountId": "5cac769747e672679f87832e",
            "amount": "20",
            "createdAt": "2019-05-13T07:20:59.739Z",
            "currency": {
                "decimal": 10,
                "id": "5c7c67de21afc513a54c7783",
                "logo": "https://cdn.sdce.com.au/images/icon-aud.png",
                "name": {
                    "en": "AUD",
                    "zh": "AUD"
                },
                "symbol": "aud",
                "type": "CURRENCY_FIAT"
            },
            "id": "5cd91adb8a7221303ef2b99b",
            "status": "DEPOSIT_STATUS_SUBMITTED",
            "type": "TRANSACTION_FIAT"
        }
    ],
    "totalCount": 2
}
```

---

### 取出记录
GET /exchange/member/{memberId}/withdraws


查询参数：
- accountId: string // AUD account ID
- pageIndex： integer// 返回取款历史列表的页数的index
- pageSize: integer //每页包含多少取款历史

返回结果：

```
{
    "result": [
        {
            "accountId": "5cac769747e672679f87832e",
            "amount": "120",
            "createdAt": "2019-05-21T00:09:08.430Z",
            "currency": {
                "decimal": 10,
                "id": "5c7c67de21afc513a54c7783",
                "logo": "https://cdn.sdce.com.au/images/icon-aud.png",
                "name": {
                    "en": "AUD",
                    "zh": "AUD"
                },
                "symbol": "aud",
                "type": "CURRENCY_FIAT"
            },
            "id": "5ce341a4e019938229018cae",
            "memberId": "5c98250fe60da8cb0eb5cba3",
            "status": "WITHDRAW_STATUS_SUBMITTED",
            "targetBank": {
                "accountId": "5cac769747e672679f87832e",
                "accountName": "yilong",
                "accountNumber": "10665248",
                "bankId": "5cc7f589d78e53b1373869d3",
                "bsb": "062667",
                "isDefault": true
            },
            "type": "TRANSACTION_FIAT",
            "withdrawFee": "0"
        }
    ],
    "totalCount": 2
}
```

---
### 创建数字货币取款地址
`POST` /exchange/member/{memberId}/account/{accountId}/withdrawFiat/fundSource
* 头部信息
  * Content-Type: application/json
  * Authorization: Bearer {accessToken}
* 路径参数
  * memberId(必填): member ID
  * accountId(必填): 取款货币账户的ID
* Body
  * address: 数字货币钱包地址
  * label: 显示的标签名称
  * makeDefault(boolean): 是否将新创建的账户作为默认取款账户

Sample request
```
https://dev-api.sdce.com.au/exchange/member/5c98250fe60da8cb0eb5cba3/account/5c99d2929224f13514cb5b8e/withdrawCoin/fundSource

body: 
{
  "address":"2N9STNWQkMdNNxDqrH12hmTbVokoy32kUX6",
  "label":"Wayne",
  "makeDefault":true
}
```

返回数据: 新建账户的ID
```json
{"id":"5d11a130831df60bc0aaaa5f"}
```

---

### 获取用户所有数字货币取款地址
`GET` /exchange/member/{memberId}/account/{accountId}/withdrawCoin/fundSource
* 头部信息
  * Content-Type: application/json
  * Authorization: Bearer {accessToken}
* 路径参数
  * memberId(必填): member ID
  * accountId(必填): 取款货币账户的ID

Sample request
```
https://dev-api.sdce.com.au/exchange/member/5c98250fe60da8cb0eb5cba3/account/5c99d2929224f13514cb5b8e/withdrawCoin/fundSource
```

返回数据: 用户所有数字货币取款地址数据
```json
{
  "Result": [
    {
      "accountId": "5c99d2929224f13514cb5b8e",
      "address": "2N9STNWQkMdNNxDqrH12hmTbVokoy32kUX6",
      "isDefault": true,
      "label": "Wayne",
      "walletId": "5d11a130831df60bc0aaaa5f"
    },
    ...
  ]
}
```

---

### 更改默认数字货币取款地址
`PUT` /exchange/member/{memberId}/account/{accountId}/withdrawCoin/fundSource/{FundSourceId}
* 头部信息
  * Content-Type: application/json
  * Authorization: Bearer {accessToken}
* 路径参数
  * memberId(必填): member ID
  * accountId(必填): 取款货币账户的ID
  * fundSourceId(必填): 对应创建的存款地址account中的walletId
* Body
  * makeDefault(boolean): 是否改为默认 

Sample request
```
https://dev-api.sdce.com.au/exchange/member/5c98250fe60da8cb0eb5cba3/account/5c99d2929224f13514cb5b8e/withdrawCoin/fundSource/5d0c91faff8703f7519bc5ba

body: 
{
  "makeDefault": true
}
```

返回数据:
```json
{
  "success": true
}
```

---

### 删除数字货币取款地址
`DELETE` /exchange/member/{memberId}/account/{accountId}/withdrawCoin/fundSource/{FundSourceId}
* 头部信息
  * Content-Type: application/json
  * Authorization: Bearer {accessToken}
* 路径参数
  * memberId(必填): member ID
  * accountId(必填): 取款货币账户的ID
  * fundSourceId(必填): 对应创建的存款地址account中的walletId

Sample request
```
https://dev-api.sdce.com.au/exchange/member/5c98250fe60da8cb0eb5cba3/account/5c99d2929224f13514cb5b8e/withdrawCoin/fundSource/5d11a130831df60bc0aaaa5f
```

返回数据:
```json
{
  "success": true
}
```

---

### 取币手续费与最小限额
`GET` exchange/withdraw/fee
* 头部信息
  * Content-Type: application/json
  * Authorization: Bearer {accessToken}
* 查询参数
  * currencies: 货币symbol

Sample request
```
https://dev-api.sdce.com.au/exchange/withdraw/fee?currencies=btc
```

返回数据:
```json
{
  "result": [
    {
      "fee": "0.0005", // 手续费
      "minimum": "0.005", // 最小取币额度
      "symbol": "btc"
    }
  ]
}
```

---

### 提交取币请求
`POST` exchange/member/{memberId}/account/{accountId}/withdrawCoin
* 头部信息
  * Content-Type: application/json
  * Authorization: Bearer {accessToken}
* 路径参数
  * memberId(必填): member ID
  * accountId(必填): 取款货币账户的ID
* Body
  * withdrawCoinRequest: { amount: 取币数量, walletId: 选择的取款地址中的walletId字段 }
  * verification: {smsCode: 收到的短信验证码, emailCode: 收到的邮件验证码 }

Sample request
```
https://dev-api.sdce.com.au/exchange/member/5c98250fe60da8cb0eb5cba3/account/5c99d2929224f13514cb5b8e/withdrawCoin

body: 
{
  "withdrawCoinRequest": {
    "amount": "9",
    "walletId": "5c9c2002ad6b1e45392daace"
  },
  "verification": {
    "smsCode": "8129",
    "emailCode": "376311"
  }
}
```

返回数据: 新建账户的ID
```json
{"id":"5d11a130831df60bc0aaaa5f"}
```

---

### 发送邮件验证码
请使用`auth0.js`

参考 https://auth0.com/docs/api/authentication#get-code-or-link

Sample code snippet:
```js
export async function sendEmail(email) {
  const webAuth = new auth0.WebAuth({
    domain,
    clientID,
  });

  return webAuth.passwordlessStart(
    {
      connection: 'email',
      send: 'code',
      email,
    },
    (err, res) => res
  );
}
```

---

### 发送短信验证码
`POST` exchange/member/sms
* 头部信息
  * Content-Type: application/json
  * Authorization: Bearer {accessToken}

Sample request
```
https://dev-api.sdce.com.au/exchange/member/sms
```

返回数据:
```json
{
  "Result": "Sucess"
}
```
