import { default as axios } from 'axios';

const sdceTestApiEndPoint = process.env. NODE_ENV === 'production' ? process.env.PROD_SDCE_ACCESS_POINT : process.env.DEV_SDCE_ACCESS_POINT

// https://api-sdce.restlet.io/#operation_search_instruments
// 查找交易对信息 比如btc, btcaud , eth…
export const searchInstrumentID = (code: string) => {
  const uri = `${sdceTestApiEndPoint}/instrument`
  const result = axios.get(uri, {
    params: {
      userInput: code,
    }
  })

  return result
}

// https://api-sdce.restlet.io/#operation_get_markets_view
// 查找所有交易对的Tickers
export const getMarketView = () => {
  const uri = `${sdceTestApiEndPoint}/instrument/tickers`
  return axios.get(uri, {
    headers: {},
  })
}

// https://api-sdce.restlet.io/#operation_ticker_info
// trading的页面，5个值：last price, 24h change , 24h volume, 24h low, 24h high
export const getOneTicker = (instrumentId: string, token: string) => {
  const uri = `${sdceTestApiEndPoint}/instrument/${instrumentId}/ticker`
  console.log(uri)
  return axios.get(uri, {
    headers: { Authorization: token },
  })
}

// https://api-sdce.restlet.io/#operation_search_orders
// 查找用户的所有交易单
export const getOrders = (token: string) => {
  const uri = `${sdceTestApiEndPoint}/orders`
  return axios.get(uri, {
    headers: { Authorization: token },
    params: {
      OnlyOpen: true,
      PageIndex: 0,
      PageSize: 200,
    },
  })
}

// https://api-sdce.restlet.io/#operation_get_public_orders
// 查找用户开放单
export const getOpenOrders = (market: string, side: string) => {
  const uri = `${sdceTestApiEndPoint}/instrument/orders`
  return axios.get(uri, {
    params: {
      instrumentId: market,
      onlyOpen: true,
      pageIndex: 0,
      pageSize: 2,
      orderSide: side
    },
  })
}

// https://api-sdce.restlet.io/#operation_get_a_order_by_id_2
// 查找指定订单
export const getOneOrder = (token: string, id: string) => {
  const uri = `${sdceTestApiEndPoint}/instrument/order/${id}`
  return axios.get(uri, {
    headers: { Authorization: token },
  })
}

// https://api-sdce.restlet.io/#operation_create_order
// 发单
export const postOneOrder = (token: string, type: string, market: string, side: string, price: string, volume: string, value: string) => {
  const uri = `${sdceTestApiEndPoint}/instrument/${market}/order`
  const priceN = Number(price)
  // price is a number in web.api
  return axios.post(uri, {
    type,
    instrument: market,
    side,
    price: priceN,
    volume,
    value,
  }, {
      headers: { Authorization: token },
    })
}

// https://api-sdce.restlet.io/#operation_cancel_order
// 取消订单
export const deleteOneOrder = (token: string, id: string) => {
  const uri = `${sdceTestApiEndPoint}/instrument/order/${id}`
  return axios.delete(uri, {
    headers: { Authorization: token },
    params: {
      reason: 'cancel order from trading bot'
    },
  })
}

// https://api-sdce.restlet.io/#operation_clear_my_orders
// 取消所有订单
export const clearOrders = (token: string) => {
  const uri = `${sdceTestApiEndPoint}/orders`
  return axios.delete(uri, {
    headers: { Authorization: token },
    params: {
      reason: 'clear orders from trading bot'
    },
  })
}
