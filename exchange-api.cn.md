# SDCE API 文档说明 / SDCE API Documentation

```
API Url:
生产环境/DEV： https://api.aho8.com
开发环境/PROD： https://dev-api.sdce.com.au
```

---

## Table of contents 目录

### 用户

- [创建用户](#创建用户)
- [获取当前用户信息](#获取当前用户信息)
- [个人设置](#个人设置)
- [KYC-IDScan证件验证](#KYC-IDScan证件验证)
- [KYC-ID3Global个人信息验证](#KYC-ID3Global个人信息验证)

### 货币

- [查找货币信息](#查找货币信息)

### 交易对（币对）Instrument

- [查找交易对信息](#查找交易对信息)
- [交易对kline](#交易对kline)
- [交易对市场深度](#交易对市场深度)
- [查找交易对订单](#查找交易对订单)
- [查找交易对已完成交易](#查找交易对已完成交易)

### 用户交易相关

- [获取当前用户指定币对订单](#获取当前用户指定币对订单)
- [创建指定币对订单](#创建指定币对订单)
- [取消未完成订单](#取消未完成订单)

### 账户与钱包

- [创建用户账户](#创建用户账户)
- [查找用户账户信息](#查找用户账户信息)
- [生成钱包地址](#生成钱包地址)
- [获取钱包地址](#获取钱包地址)
- [创建法币取款地址](#创建法币取款地址)
- [获取法币取款地址](#获取法币取款地址)
- [更改法币默认取款地址](#更改法币默认取款地址)
- [删除法币取款地址](#删除法币取款地址)
- [创建数字货币取款地址](#创建数字货币取款地址)
- [获取数字货币取款地址](#获取数字货币取款地址)
- [更改数字货币默认取款地址](#更改数字货币默认取款地址)
- [删除数字货币取款地址](#删除数字货币取款地址)
- [提币与取现费用](#提币与取现费用)
- [查询提币与取现限额](#查询提币与取现限额)
- [发送短信验证码](#发送短信验证码)
- [发送邮件验证码](#发送邮件验证码)
- [提取法币](#提取法币)
- [提取数字货币](#提取数字货币)

### 历史查询

- [查询存入历史](#查询存入历史)
- [查询取出历史](#查询取出历史)
- [查询订单历史](#查询订单历史)
- [查询交易历史](#查询交易历史)

### OTC

- [获取OTC交易对](#获取OTC交易对)
- [获取OTC报价信息](#获取OTC报价信息)
- [查询OTC公开订单](#查询OTC公开订单)
- [查询用户OTC交易订单](#查询用户OTC交易订单)
- [获取用户OTC订单详情](#获取用户OTC订单详情)
- [修改OTC个人信息](#修改OTC个人信息)
- [创建OTC报价](#创建OTC报价)
- [发起OTC交易(买入)](#发起OTC交易(买入))
- [发起OTC交易(卖出)](#发起OTC交易(卖出))
- [标记为场外已付款](#标记为场外已付款)
- [标记为OTC卖家已释放](#标记为OTC卖家已释放)
- [取消OTC订单](#取消OTC订单)

### BOLT

- [获取兑换报价信息](#获取兑换报价信息)
- [创建兑换订单](#创建兑换订单)
- [获取兑换订单详情](#获取兑换订单详情)
- [获取兑换订单历史](#获取兑换订单历史)
- [创建CNY收付款信息](#创建CNY收付款信息)
- [创建USDT收付款信息](#创建USDT收付款信息)
- [创建AUD/USD收付款信息](#创建AUD/USD收付款信息)
- [确认兑换订单收付款信息](#确认兑换订单收付款信息)
- [上传支付截图并确认](#上传支付截图并确认)

### 其他

- [上传文件](#上传文件)

- 存款/存币
  - 数字货币地址可在账户信息中找到 [查找用户账户信息](#查找用户账户信息)
  - [澳币通过 POLi 或银行转帐 ](#存入澳币Poli或转账)
- 账户记录
  - [存入记录](#存入记录)
  - [取出记录](#取出记录)

### 换汇流程(CNY -> 澳币/美元)

- [填写提交换汇金额](#填写提交换汇金额)
- 确认订单
  - [验证银行卡号](#验证银行卡号)
  - [激活换汇订单](#激活换汇订单)

---

## API 说明

### 创建用户

`POST` /exchange/member

- Description: 用户通过 Auth0 注册之后，使用 accessToken 在 SDCE 系统创建新用户
- 头部信息 header :
  - Content-Type: application/json
  - Authorization: Bearer {accessToken}
- 路径参数 path parameters :

- Body

  - contact: 联系信息
  - email: 邮箱地址
  - refCode: 邀请码

- Sample request

  ```
  https://dev-api.sdce.com.au/exchange/member

  body:
  {
      "contact": {
          "email": "ao.liu+test1@sdce.com.au"
      },
      "refCode": ""
  }
  ```

- 返回数据: 新建用户的 ID
- Sample response:

  ```json
  { "Result": "5d12d3cb4b3093cb3de9d733" }
  ```

---

### 获取当前用户信息

`GET` /exchange/member

- Description: 登录用户使用 access token 获取用户的信息
- 头部信息 header :

  - Content-Type: application/json
  - Authorization: Bearer {accessToken}

- Sample request :

  ```
  https://dev-api.sdce.com.au/exchange/member
  ```

- Sample response:

  ```json
  {
    "authid": "auth0|5c98250953fb160ec8e9b96f",
    "contact": {
      "email": "john.doe+super@sdce.com.au",
      "phoneNumber": "+610420390409",
      "phoneVerified": true
    },
    "createdAt": "2019-03-25T00:47:11.681Z",
    "director": {
      "declarePhoto": "/kyc/7820492c-aa03-4105-9b44-d88b3705895a_KYC.jpg",
      "registrantPhotoIdBack": "/kyc/37f8cfdd-b8f0-4865-aaed-223921c057f4_KYC.jpg",
      "registrantPhotoIdFront": "/kyc/67b97c71-2e91-4822-b4b9-bbe38253e5d1_KYC.jpg"
    },
    "id": "5c98250fe60da8cb0eb5cba3", // member id
    "inviteCode": "1933075630", // used for inviting a new member, will be used as the 'refCode' when creating a new user
    "kycLevel": "KYC_INVALID_LEVEL", // kyc info
    "memberLevel": {
      "coinDepositLimit": "0",
      "coinWithdrawLimit": "0",
      "depositAudLimit": "0",
      "level": 0,
      "withdrawAudLimit": "0"
    },
    "otcDetail": {
      // payment information forOTCtrade
      "alipayDetail": {
        "QRCode": "/otc/c265e19e-51f2-4db3-b6ef-ad1ca2b9367a_0po3o52qnps0470onp3nrq57oqrso84q.jpg",
        "accountNumber": "支付宝85742396",
        "name": "庄晓曼"
      },
      "bankPaymentDetail": {
        "accountNumber": "6230580000083608078",
        "bankBranchInfo": "学院路支行",
        "firstName": "途",
        "lastName": "肖"
      },
      "nickName": "庄晓曼",
      "wechatPaymentDetail": {
        "QRCode": "/otc/e9cacd37-9429-49a8-ac66-8390bde4d2da_wechatpay.png",
        "accountNumber": "晓曼的微信"
      }
    },
    "otcFeeRate": "0.002", //OTCtransaction fee
    "portfolio": {
      "assets": [
        {
          "balance": "990",
          "currency": "USDT"
        },
        {
          "balance": "197250",
          "currency": "ETH"
        },
        {
          "balance": "4658415",
          "currency": "BTC"
        }
      ],
      "audTxFeeRate": "0.0010",
      "tokenTxRate": "0.0022",
      "total30dTradeAudVal": "82997061270008"
    },
    "referralSummary": {
      "commissionRate": "0.20",
      "referrals": [
        {
          "email": "yulan.doe+test1@sdce.com.au",
          "id": "5cb7c298021f35c478cf9118",
          "joinedDate": "2019-04-18T00:19:36.684Z"
        },
        {
          "email": "john.doe+ref3@sdce.com.au",
          "id": "5cb7c2a454065101ac35e9f3",
          "joinedDate": "2019-04-18T00:19:48.756Z"
        },
        {
          "email": "john.doe+ref4@sdce.com.au",
          "id": "5cb805dcb9c04babef8e9587",
          "joinedDate": "2019-04-18T05:06:36.500Z"
        },
        {
          "email": "john.doe+refer1@sdce.com.au",
          "id": "5cbe8891fa14697e6e3de4c4",
          "joinedDate": "2019-04-23T03:37:53.606Z"
        },
        {
          "email": "john.doe+refer2@sdce.com.au",
          "id": "5cbe8a240d8b094ef8c5b08e",
          "joinedDate": "2019-04-23T03:44:36.816Z"
        }
      ]
    },
    "registrant": {
      "address": {
        "address1": "56 Pitt"
      },
      "dob": "1990-01-01",
      "firstName": "郎",
      "idNumber": "123456789",
      "lastName": "小",
      "middleName": "三"
    },
    "source": "International",
    "type": "Individual",
    "updatedAt": "2019-03-25T00:47:11.681Z",
    "walletUID": 120
  }
  ```

---

### 个人设置

`POST` /exchange/member/{memberId}/status

- 头部信息 header :
  - Content-Type: application/json
  - Authorization: Bearer {accessToken}
- 路径参数 path parameters :

  - memberId

- Body
  - enableMfa: 开启 mfa
  - verifyEmail: 验证邮箱
  - fieldMask: 部分更新 mask，标示 body 中含有的 key 名，如"verifyEmail,enableMfa"
  - phone: 电话
  - smsCode: 短信验证码

Sample request

```
https://dev-api.sdce.com.au/exchange/member/5d12d3cb4b3093cb3de9d635/status

body:
{
    "verifyEmail": true,
    "fieldMask": "verifyEmail"
}
```

返回数据: 更新结果

```json
{ "success": true }
```

---

### KYC-IDScan证件验证

`POST` /exchange/idscan/journey

- Description: 上传护照或者大陆身份证证件正面、反面 及 手持证件与声明的自拍（按顺序上传三张图片），得到证件图片是否通过验证的结果以及证件提取信息

  - 此步骤之前必须先完成手机绑定/验证
  - `注意： 图片通过base64转码的格式发送`

- 头部信息 header :

  - Content-Type: application/json
  - Authorization: Bearer {accessToken}

- Body
  - memberId
  - request
    - additionalData: 数组，[{ "Name": "member_id", "Value": memberId }]
    - excludeOutputImagesFromResponse: "true"(注意为字符串)，排除返回结果中的图片信息
    - identityMeansId: memberId
    - inputImages: 数组，三张图片的信息，其中每张图片的信息为一个对象，如下
      - name: "WhiteImage"，字符串，固定表示普通图片
      - imageFormat: 上传图片的格式，字符串，格式可为 jpeg/jpg/png
      - data: 图片的 base64 转码

* Sample request

  ```
  https://api.aho8.com/exchange/idscan/journey
  ```

  - Request Body

  ```json
  {
    "memberId": "5ebe0f6b1586cf508413bff8",
    "request": {
      "additionalData": [
        {
          "Name": "member_id",
          "Value": "5ebe0f6b1586cf508413bff8"
        }
      ],
      "excludeOutputImagesFromResponse": "true",
      "identityMeansId": "5ebe0f6b1586cf508413bff8",
      "inputImages": [
        {
          "name": "WhiteImage",
          "imageFormat": "jpeg",
          "data": "/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAAQDAwQDAwQEAwQFBAQFBgoHBgYGBg0JCg......"
        },
        {
          "name": "WhiteImage",
          "imageFormat": "jpeg",
          "data": "/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDABIMDhAOCxIQDxAUExIVGy0dGxkZGzcoKi......"
        },
        {
          "name": "WhiteImage",
          "imageFormat": "jpeg",
          "data": "/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDABIMDhAOCxIQDxAUExIVGy0dGxkZGzcoKiE......"
        }
      ],
      "isDocumentExtracted": "false",
      "personEntryId": "5ebe0f6b1586cf508413bff8",
      "source": 2
    }
  }
  ```

- 返回数据

  - HighLevelResult：若为"Passed"表示证件图片验证通过
  - EntryData: 证件图片的验证信息(JSON string 形式)，包括字段如下，其中包含 ExtractedFields 的字段表示证件中提取信息，可以用作下一步[个人信息验证](#KYC-ID3Global个人信息验证)的预填信息
    - LivenessMaxFail
    - LivenessActionTimeout
    - LivenessPassedFrames
    - DocumentType
    - UserName
    - UnderAgeRuleResult
    - CountryCode
    - RequiredJourneySteps
    - LivenessNumberOfSelfie
    - LivenessJumpsAllowed
    - LivenessFramesNumber
    - ScanReference
    - LivenessActionsNumber
    - AuthenticationLevel
    - 'ExtractedFields.DocumentType'
    - 'ExtractedFields.NationalityName'
    - 'ExtractedFields.MRZFull'
    - 'ExtractedFields.FirstName'
    - 'ExtractedFields.ExpiryDate'
    - 'ExtractedFields.IssuingLocation'
    - 'ExtractedFields.MRZLine2'
    - 'ExtractedFields.PersonalNumber'
    - 'ExtractedFields.FullName'
    - 'ExtractedFields.IssuingAuthority'
    - 'ExtractedFields.LastName'
    - 'ExtractedFields.Sex'
    - 'ExtractedFields.IssueDate'
    - 'ExtractedFields.DocumentCategory'
    - 'ExtractedFields.BirthPlace'
    - 'ExtractedFields.MRZLine1'
    - 'ExtractedFields.DocumentNumber'
    - 'ExtractedFields.NationalityCode'
    - 'ExtractedFields.BirthDate'
    - 'QualityChecks.LowResolutionCheck'
    - 'QualityChecks.GlareCheck'
    - 'QualityChecks.FullDocumentInViewCheck'
    - 'QualityChecks.BlurCheck'

  ```json
  {
    "memberId": "5ebe0f6b1586cf508413bff8",
    "result": {
      "CurrentResult": "Pass",
      "EntryData": "{\"LivenessMaxFail\":\"1\",...,\"QualityChecks.LowResolutionCheck\":\"Good\",\"QualityChecks.GlareCheck\":\"Good\",\"QualityChecks.FullDocumentInViewCheck\":\"Good\",\"QualityChecks.BlurCheck\":\"Good\"}",
      "EntryDateTime": "2020-06-03T06:27:19.260Z",
      "HighLevelResult": "Passed",
      "PersonEntryId": "6b483861-44ea-4a4a-8a00-62078c718d14",
      "RequiredAction": "LIVENESS",
      "ResultDetails": [
        "DOCUMENTFRONTSIDETYPECHECK:PASSED",
        "UNDERAGERULE:PASSED",
        "DOCUMENTBACKSIDECHECK:NOTNEEDED",
        "DOCUMENTBLOCKINGPOLICY:PASSED",
        "DOCUMENTEXPIRY:PASSED",
        "DOCUMENTSUPPORT:PASSED",
        "DOCUMENTPROOFPOLICY:SKIPPED",
        "DOCUMENTVALIDATION:PASSED"
      ]
    }
  }
  ```

---

### KYC-ID3Global个人信息验证

`POST` /exchange/member/{memberId}/kyc

- Description: 发送个人和证件信息通过身份验证

  - 此步骤之前必须先完成[KYC-IDScan](#KYC-IDScan证件验证)
  - 证件信息必须与[KYC-IDScan](#KYC-IDScan证件验证)步骤中的信息相一致

- 头部信息 header :

  - Content-Type: application/json
  - Authorization: Bearer {accessToken}

- Body
  - profileID: 护照为"e78b6c6e-ed8a-4e96-bb78-66ec88cd28ab"， 大陆身份证为"155d5823-40e5-4bc9-b2b5-d2bcd90003e4"
  - kycDetails: 个人信息和证件信息的JSON string，护照和身份证的格式(JSON parse之后)分别为
    - 护照, `注意: InternationalPassport.Number不是护照号，而是护照为下方的机器可识别码`，参见[KYC-IDScan](#KYC-IDScan证件验证)response中的`'ExtractedFields.MRZLine2'`字段
      ```json
      {
        "Personal": {
          "PersonalDetails": {
            "Surname": "xxx", // 姓
            "Forename": "xxxx", // 名
            "Gender": "Male", // 性别
            "DOBDay": 28, // 出生日
            "DOBMonth": 9, // 出生月
            "DOBYear": 1985 // 出生年
          }
        },
        "IdentityDocuments": {
          "InternationalPassport": {
            "Number": "E865xxxx7CHN80xxxcMFONMLMONGLOA962", // ExtractedFields.MRZLine2
            "CountryOfOrigin": "China", // 签发国家
            "ExpiryDay": 30, // 过期日
            "ExpiryMonth": 10, // 过期月
            "ExpiryYear": 2029 // 过期年
          }
        }
      }
      ```
    - 大陆身份证
      ```json
      {
        "Personal": {
          "PersonalDetails": {
            "Surname": "WU", // 姓
            "Forename": "JUNRU", // 名
            "Gender": "Male", // 性别
            "CountryOfBirth": "China",
            "DOBDay": 18, // 出生日
            "DOBMonth": 10, // 出生月
            "DOBYear": 1985 // 出生年
          }
        },
        "IdentityDocuments": {
          "China": {
            "ResidentIdentityCard": {
              "Number": "321xxxx879xxxxxx62953" // 身份证号
            }
          }
        },
        "Addresses": {
          "CurrentAddress": {
            "Country": "China"
          }
        }
      }
      ```


* Sample request

  ```
  https://api.aho8.com/exchange/member/5ebe0f6b1586cf508413bff8/kyc
  ```

  - Request Body

  ```json
  // -------- 使用护照 -----------
  {
    "kycDetails": "{\"Personal\":{\"PersonalDetails\":{\"Surname\":\"xxx\",\"Forename\":\"xxxx\",\"Gender\":\"Male\",\"DOBDay\":28,\"DOBMonth\":9,\"DOBYear\":1985}},\"IdentityDocuments\":{\"InternationalPassport\":{\"Number\":\"E865xxxx7CHN80xxxcMFONMLMONGLOA962\",\"CountryOfOrigin\":\"China\",\"ExpiryDay\":30,\"ExpiryMonth\":10,\"ExpiryYear\":2029}}}",
    "profileID": "e78b6c6e-ed8a-4e96-bb78-66ec88cd28ab"
  }

  // 或者
  // -------- 使用身份证 -----------
  {
    "kycDetails": "{\"Personal\":{\"PersonalDetails\":{\"Surname\":\"WU\",\"Forename\":\"JUNRU\",\"Gender\":\"Male\",\"CountryOfBirth\":\"China\",\"DOBDay\":16,\"DOBMonth\":10,\"DOBYear\":1985}},\"IdentityDocuments\":{\"China\":{\"ResidentIdentityCard\":{\"Number\":\"321xxxx879xxxxxx62953\"}}},\"Addresses\":{\"CurrentAddress\":{\"Country\":\"China\"}}}",
    "profileID": "155d5823-40e5-4bc9-b2b5-d2bcd90003e4"
  }
  ```

- 返回数据
  - updateKYCLevel: 用户KYC状态，'KYC_IDENTITY_VERIFIED'表示通过，'KYC_IDENTITY_INVALID'表示未通过

  ```json
  {
    "updateKYCLevel": "KYC_IDENTITY_VERIFIED"
  }
  ```

---

### 获取当前用户指定币对订单

`GET` /exchange/orders

- Description: 登录用户使用 access token 获取自己在指定币对的订单数据
- 头部信息 header :
  - Content-Type: application/json
  - Authorization: Bearer {accessToken}
- 查询参数 query parameters :

  - instrument: instrument ID（币对 ID）
  - member: member ID
  - onlyOpen: true/false 是否是开放（open）订单
  - pageIndex: 第几页，从 0 开始
  - pageSize: 每页数据条数

- Sample request :

  ```
  https://api.aho8.com/exchange/orders?instrument=5c947d44da0d2c6c16ef7aed&member=5c947d3eda0d2c6c16ef79cd&pageIndex=0&pageSize=100&onlyOpen=true
  ```

- Sample response:

  ```json
  {
    "result": [
      {
        "events": null,
        "filled_value": "0",
        "filled_volume": "0",
        "id": "5e13fdc55bc2958d1fb9ab8a",
        "instrument": {
          "base": {
            "decimal": 8,
            "id": "5c947d3eda0d2c6c16ef7a68",
            "logo": "https://icons-exchange.oss-ap-southeast-2.aliyuncs.com/icon-btc.png",
            "name": {
              "en": "BTC",
              "zh": "BTC"
            },
            "symbol": "btc",
            "type": "CURRENCY_BTC"
          },
          "code": "btcaud",
          "id": "5c947d44da0d2c6c16ef7aed",
          "name": "BTC AUD",
          "quote": {
            "decimal": 10,
            "id": "5c947d3eda0d2c6c16ef7a67",
            "logo": "https://icons-exchange.oss-ap-southeast-2.aliyuncs.com/icon-aud.png",
            "name": {
              "en": "AUD"
            },
            "symbol": "aud",
            "type": "CURRENCY_FIAT"
          }
        },
        "owner": {
          "clientId": "5c947d3eda0d2c6c16ef79cd"
        },
        "price": 200,
        "side": "BID",
        "status": "OPEN",
        "time": "2020-01-07T03:40:53.695Z",
        "type": "LIMIT",
        "value": "200",
        "volume": "1"
      }
    ],
    "totalCount": 1
  }
  ```

---

### 创建指定币对订单

`POST` /exchange/instrument/{instrumentId}/order

- Description: 创建一个指定币对(e.g. BTC AUD)的交易订单
- 头部信息 header :
  - Content-Type: application/json
  - Authorization: Bearer {accessToken}
- 路径参数 path parameters :
  - instrumentId: 币对 ID
- Body:

  - side: String // BID | ASK
  - type: String // MARKET | LIMIT (目前只支持这两种订单)
  - price： number // 订单价格
  - volume： String // 交易量， 卖单必填， 买单选填
  - value： String // 交易价值， 买单必填， 卖单选填

- Sample request :

  ```json
  https://api.aho8.com/exchange/instrument/5c947d44da0d2c6c16ef7aed/order

  body:
  {
    "side": "BID",
    "type": "LIMIT",
    "price": 10923,
    "volume": "0.032",
    "value": "349.536"
  }
  ```

- 返回结果: 新创建订单 ID
- Sample response:
  ```json
  {
    "Result": "5e1409a35bc2958d1fb9b6b7"
  }
  ```

---

### 修改OTC个人信息

`PUT` /exchange/otc/member/otcDetails

- Description: 修改用户OTC板块的个人信息，包括昵称，支付宝/微信/银行收款信息
- 头部信息 header :
  - Content-Type: application/json
  - Authorization: Bearer {accessToken}
- Body:

  - fieldMask(String): 选择以下其中一项
    - otcDetails.nickName
    - otcDetails.wechatPaymentDetail
    - otcDetails.alipayDetail
    - otcDetails.bankPaymentDetail
  - nickName(string): 当 fieldMask 为 otcDetails.nickName
  - wechatPaymentDetail(Object): 当 fieldMask 为 otcDetails.wechatPaymentDetail
    - accountNumber(String)
    - QRCode(String)
  - alipayDetail(Object): 当 fieldMask 为 otcDetails.alipayDetail
    - name(String)
    - accountNumber(String)
    - QRCode(String)
  - bankPaymentDetail(Object): 当 fieldMask 为 otcDetails.bankPaymentDetail
    - accountNumber(String)
    - bankBranchInfo(String)
    - firstName(String)
    - lastName(String)

- Sample request :

  ```json
  https://dev-api.sdce.com.au/exchange/otc/member/otcDetails

  body:
  {
    "nickName": "庄晓曼",
    "fieldMask": "otcDetails.nickName"
  }
  ```

- Sample request #2:

  ```json
  https://dev-api.sdce.com.au/exchange/otc/member/otcDetails

  body:
  {
    "alipayDetail": {
      "name": "庄晓曼",
      "accountNumber": "18585742396",
      "QRCode": "/otc/dd2e1143-0290-4727-90a6-8129edad9f40_0po3o52qnps0470onp3nrq57oqrso84q.jpg"
    },
    "fieldMask": "otcDetails.alipayDetail"
  }
  ```

- Sample request #3:

  ```json
  https://dev-api.sdce.com.au/exchange/otc/member/otcDetails

  body:
  {
    "wechatPaymentDetail": {
      "accountNumber": "晓曼的微信",
      "QRCode": "/otc/39a2bdbc-bca3-491b-a305-323d562fe4d6_wechatpay.png"
    },
    "fieldMask": "otcDetails.wechatPaymentDetail"
  }
  ```

- Sample request #4 :

  ```json
  https://dev-api.sdce.com.au/exchange/otc/member/otcDetails

  body:
  {
    "bankPaymentDetail": {
      "accountNumber": "6230580000083608078",
      "bankBranchInfo": "学院路支行",
      "firstName": "庄",
      "lastName": "晓曼"
    },
    "fieldMask": "otcDetails.bankPaymentDetail"
  }
  ```

- Sample response:
  ```json
  { "result": "Updated" }
  ```

---

### 关闭OTC报价

`DELETE` https://dev-api.sdce.com.au/exchange/otc/quote/{quoteId}

- Description: 关闭用户的OTC交易报价
- 头部信息 header :
  - Content-Type: application/json
  - Authorization: Bearer {accessToken}
- 路径参数 Path parameters:
  - quoteId: 对应报价 quote ID
- Sample request:
  ```
  https://dev-api.sdce.com.au/exchange/otc/quote/5e30e0733ce49593167fc4ea
  ```
- Sample response:
  ```json
  { "code": "202", "message": "success" }
  ```

---

### 创建OTC报价

`POST` /exchange/otc/quote

- Description: 创建一个OTC交易报价
- 头部信息 header :
  - Content-Type: application/json
  - Authorization: Bearer {accessToken}
- Body:

  - base: 基准货币，e.g. 'btc',
  - quote: 报价货币，e.g. 'cny'
  - side: 'BID'|'ASK'
  - volume: 交易量
  - oneTransaction: 是否是整单交易
    - 整单交易表示必须一次交易报价的全部交易量
    - 否为'0'
    - 是为'1'
  - price: 13200
  - order_live_time: 报价进入交易状态后过期时间，以秒为单位
  - max_value: 单笔最大交易金额
  - min_value: 单笔最小交易金额
  - accepted_payment_methods:
    - 数组，内为卖家接受的场外支付方式选项，仅在 quote 为 cny 时填写
    - e.g. ['WECHAT', 'ALIPAY', 'BANK']
  - type:
    - 'REGULAR' | 'WHOLESALE'， 分别表示普通报价 或者 整单交易报价
  - status: 订单状态，创建时为'ON'
  - note: 留言信息

- Sample request :

  ```json
  https://dev-api.sdce.com.au/exchange/otc/quote

  body:
  {
    "base": "btc",
    "quote": "cny",
    "side": "ASK",
    "volume": "0.2",
    "oneTransaction": "0",
    "price": 13200,
    "order_live_time": 900,
    "max_value": "2640.00",
    "min_value": "1000",
    "accepted_payment_methods": [
      "WECHAT",
      "ALIPAY",
      "BANK"
    ],
    "type": "REGULAR",
    "status": "ON"
  }
  ```

- Sample request #2 :

  ```json
  https://dev-api.sdce.com.au/exchange/otc/quote

  body:
  {
    "base": "btc",
    "quote": "aud",
    "side": "BID",
    "volume": "0.2",
    "oneTransaction": "1",
    "price": 11500,
    "order_live_time": 900,
    "max_value": "2300",
    "notes": "some notes",
    "min_value": "2300",
    "value": "2300",
    "type": "WHOLESALE",
    "status": "ON"
  }
  ```

- 返回结果: 新创建报价 ID
- Sample response:
  ```json
  { "id": "5e2f80f23ce49593167fc4e8" }
  ```

---

### 发起OTC交易(买入)

`POST` /exchange/otc/quote/{quoteId}/buy

- Description: 发起一笔OTC报价的买入交易
- 头部信息 header :
  - Content-Type: application/json
  - Authorization: Bearer {accessToken}
- 路径参数 Path parameters:
  - quoteId: 对应报价 quote ID
- Body:

  - value: 交易金额
  - volume: 交易量
  - quote: 对应报价 quote ID
  - action: 'buy'

- Sample request :

  ```json
  https://dev-api.sdce.com.au/exchange/otc/quote/5e2f80f23ce49593167fc4e8/buy

  body:
  {
    "value": "1320.00",
    "volume": "0.1",
    "quote": "5e2f80f23ce49593167fc4e8",
    "action": "buy"
  }
  ```

- 返回结果: 新创建交易 ID
- Sample response:
  ```json
  { "orderId": "5e30e14b3ce49593167fc4ec" }
  ```

---

### 发起OTC交易(卖出))

`POST` /exchange/otc/quote/{quoteId}/buy

- Description: 发起一笔OTC报价的卖出交易
- 头部信息 header :
  - Content-Type: application/json
  - Authorization: Bearer {accessToken}
- 路径参数 Path parameters:
  - quoteId: 对应报价 quote ID
- Body:

  - value: 交易金额
  - volume: 交易量
  - quote: 对应报价 quote ID
  - action: 'sell'

- Sample request :

  ```json
  https://dev-api.sdce.com.au/exchange/otc/quote/5e2f86273ce49593167fc4e9/sell

  body:
  {
    "value": "2300",
    "volume": "0.2",
    "quote": "5e2f86273ce49593167fc4e9",
    "action": "sell"
  }
  ```

- 返回结果: 新创建交易 ID
- Sample response:
  ```json
  { "orderId": "5e30e3bd3ce49593167fc4f2" }
  ```

---

### 标记为场外已付款

`PUT` /exchange/otc/order/{orderId}

- Description:OTC场外交易，买家付款后标记订单状态为已经付款
- 路径参数 Path parameters:
  - orderId: 对应交易订单 order ID
- Body:
  - status: "PAID"
- Sample request

  ```json
  https://dev-api.sdce.com.au/exchange/otc/order/5e30e14b3ce49593167fc4ec

  { "status": "PAID" }
  ```

- Sample response:
  ```json
  { "code": "202", "message": "updated" }
  ```

---

### 标记为OTC卖家已释放

`PUT` /exchange/otc/order/{orderId}

- Description:OTC场外交易，卖家收款后释放货币并标记为已经释放
- 路径参数 Path parameters:
  - orderId: 对应交易订单 order ID
- Body:
  - status: "COMPLETED"
- Sample request

  ```json
  https://dev-api.sdce.com.au/exchange/otc/order/5e30e4683ce49593167fc4f6

  {"status":"COMPLETED"}
  ```

- Sample response:
  ```json
  { "code": "202", "message": "updated" }
  ```

---

### 取消OTC订单

`PUT` /exchange/otc/order/{orderId}

- Description: 取消当前的一笔OTC场外交易
- 路径参数 Path parameters:
  - orderId: 对应交易订单 order ID
- Body:
  - status: "CANCELLED"
- Sample request

  ```json
  https://dev-api.sdce.com.au/exchange/otc/order/5e30e3bd3ce49593167fc4f2

  {"status":"CANCELLED"}
  ```

- Sample response:
  ```json
  { "code": "202", "message": "canceled" }
  ```

---

### 上传文件

- Description: 上传图片/PDF 等文件
- Step1: [生成一个上传 URL](#生成一个上传URL)，得到一个 URL 的返回值
- Step2: 使用 Step1 中返回的 URL，使用`PUT`方法上传文件
  - 头部信息 header :
    - Content-Type: application/json
    - Authorization: Bearer {accessToken}
- Step3: 存储为 Step1 中 category 和 fileName 组合的 /{category}/{fileName} 形式的字符串，e.g. '/otc/55129276-9281-4748-8900-a09cb0469035_wechatpay.png'
  - 头部信息 header :
    - Content-Type: application/json
    - Authorization: Bearer {accessToken}

---

### 生成一个上传 URL

`POST` /exchange/document/sign-url

- Description: 为上传文件生成一个 URL 地址
- 头部信息 header :
  - Content-Type: application/json
  - Authorization: Bearer {accessToken}
- Body:
  - fileName: 唯一的文件名
  - category: 上传文件所属类型，例如 otc
- Sample request
  ```json
  https://dev-api.sdce.com.au/exchange/document/sign-url
  {
    "fileName": "55129276-9281-4748-8900-a09cb0469035_wechatpay.png",
    "category": "otc"
  }
  ```
- Sample response
  ```json
  {
    "url": "https://dev-backend-exchange.oss-ap-southeast-2.aliyuncs.com/otc%2F55129276-9281-4748-8900-a09cb0469035_wechatpay.png?Expires=1580256520&OSSAccessKeyId=LTAIOrXyjH5fmV8q&Signature=5NcPPV6vCPZulL87x8TAC56M8X0%3D"
  }
  ```

---

### 获取上传文件地址

`GET` /exchange/document/{category}/{fileName}

- Description：根据数据库存储的文件名获取真实的文件访问地址
- 头部信息 header :
  - Content-Type: application/json
  - Authorization: Bearer {accessToken}
- 路径参数 path parameters :
  - fileName: 唯一的文件名，例如'/otc/55129276-9281-4748-8900-a09cb0469035_wechatpay.png'中的'otc'
  - category: 上传文件所属类型，例如'/otc/55129276-9281-4748-8900-a09cb0469035_wechatpay.png'中的'55129276-9281-4748-8900-a09cb0469035_wechatpay.png'
- Sample request
  ```
  https://dev-api.sdce.com.au/exchange/document/otc/55129276-9281-4748-8900-a09cb0469035_wechatpay.png
  ```
- 返回数据：文件真实访问地址
- Sample response
  {
  "url": "https://dev-backend-exchange.oss-ap-southeast-2.aliyuncs.com/otc%2F55129276-9281-4748-8900-a09cb0469035_wechatpay.png?Expires=1580260261&OSSAccessKeyId=LTAIOrXyjH5fmV8q&Signature=nnHKNOjxSZgqzmITSSeS9Z43IcQ%3D"
  }

  ```

  ```

---

### 取消未完成订单

`DELETE` /exchange/instrument/order/{orderId}

- Description: 取消用户的一笔未完成订单
- 头部信息 header :
  - Content-Type: application/json
  - Authorization: Bearer {accessToken}
- 路径参数 path parameters :
  - orderId: 订单 ID
- 查询参数 query parameters :

  - reason: 取消原因，e.g. no_reason

- Sample request :

  ```
  https://api.aho8.com/exchange/instrument/order/5e13fdc55bc2958d1fb9ab8a?reason=no_reason
  ```

---

### 获取OTC报价信息

`GET` /exchange/otc/quote

- Description: 获取当前
- 头部信息 header :
  - Content-Type: application/json
- 查询参数 query parameters :

  - status: 可选值为
    - ON
    - OFF
    - CLOSED
    - EXPIRED
  - side: ASK | BID
  - baseSymbol: e.g., btc
  - quoteSymbol: e.g., aud
  - pageIdx: 第几页，从 0 开始
  - pageSize: 每页数据条数
  - userId: member ID

- Sample request :
  ```
  https://dev-api.sdce.com.au/exchange/otc/quote?pageIdx=0&pageSize=10&side=ASK&baseSymbol=btc&status=ON
  ```
- Sample response:
  ```json
  {
    "quotes": [
      {
        "accepted_payment_methods": [
          "ALIPAY",
          "WECHAT"
        ],
        "events": [
          {
            "price": 0.01,
            "time": "2019-07-25T06:49:14.509Z",
            "type": "CREATE_ORDER",
            "value": "100",
            "volume": "10020"
          },
          {
            "time": "2019-09-04T04:44:36.342Z",
            "type": "UPDATE_ORDER",
            "value": "0",
            "volume": "0"
          },
          {
            "time": "2019-09-04T05:06:05.014Z",
            "type": "UPDATE_ORDER",
            "value": "100",
            "volume": "10000"
          },
          {
            "time": "2019-09-04T05:06:26.875Z",
            "type": "UPDATE_ORDER",
            "value": "0",
            "volume": "0"
          },
          {
            "time": "2019-09-04T05:22:05.016Z",
            "type": "UPDATE_ORDER",
            "value": "100",
            "volume": "10000"
          },
          {
            "time": "2019-10-13T22:57:33.345Z",
            "type": "UPDATE_ORDER",
            "value": "0",
            "volume": "0"
          },
          {
            "time": "2019-10-13T23:13:21.031Z",
            "type": "UPDATE_ORDER",
            "value": "100",
            "volume": "10000"
          }
        ],
        "id": "5d3950ea03d7f2e0ee0031dd",
        "instrument": {
          "base": {
            "decimal": 8,
            "id": "5c7c67dd21afc513a54c7782",
            "logo": "https://upload.wikimedia.org/wikipedia/commons/9/9a/BTC_Logo.svg",
            "name": {
              "en": "BTC",
              "zh": "BTC"
            },
            "symbol": "btc",
            "type": "CURRENCY_BTC"
          },
          "code": "btccny",
          "id": "5cb3c93921e6d05d4357f125",
          "name": "BTC CNY",
          "quote": {
            "decimal": 2,
            "id": "5cb191c845d26492c0c28b21",
            "logo": "https://cdn3.iconfinder.com/data/icons/currency-2/460/Yuan-512.png",
            "name": {
              "en": "CNY",
              "zh": "CNY"
            },
            "symbol": "cny",
            "type": "CURRENCY_FIAT"
          }
        },
        "last_updated_time": "2019-07-25T06:49:14.479Z",
        "lockedFee": "0.0000002",
        "max_value": "1",
        "min_value": "1",
        "order_live_time": 900,
        "owner": "5d145c387d797b855d7be9f2",
        "owner_otc_detail": {
          "alipayDetail": {
            "QRCode": "/otc/53604125-0a92-4517-a66e-ccaaccc04056_card.png",
            "accountNumber": "4879254325",
            "name": "武藤纯子的alipay"
          },
          "nickName": "武藤纯子",
          "wechatPaymentDetail": {
            "QRCode": "/otc/fa7e206b-c862-45b0-a057-642547cec4cc_0po3o52qnps0470onp3nrq57oqrso84q.jpg",
            "accountNumber": "武藤纯子的wechat"
          }
        },
        "price": 10000,
        "processing_volume": "0",
        "side": "ASK",
        "status": "ON",
        "time": "1970-01-01T00:00:00.000Z",
        "type": "WHOLESALE",
        "value": "1",
        "volume": "0.0001"
      },
      ...
    ],
    "totalCount": 8
  }
  ```

---

### 查询OTC公开订单

`GET` /exchange/otc/order/public

- Description: 查询用户所有的存入/转入历史记录
- 头部信息 header :
  - Content-Type: application/json
  - Authorization: Bearer {accessToken}
- 查询参数 query parameters :

  - statusArray: 筛选状态
  - pageIdx: 第几页，从 0 开始
  - pageSize: 每页数据条数
  - side
  - baseSymbol
  - quoteSymbol

- Sample request :

  ```
  https://dev-api.sdce.com.au/exchange/otc/order/public?statusArray=COMPLETED&pageIdx=0&pageSize=10
  ```

- Sample response:
  ```json
  {
    "result": [
      {
        "accountId": "5cac769747e672679f87832e",
        "amount": "10000",
        "createdAt": "2019-07-10T07:57:08.537Z",
        "currency": {
          "decimal": 10,
          "id": "5c7c67de21afc513a54c7783",
          "logo": "https://cdn.sdce.com.au/images/icon-aud.png",
          "name": {
            "en": "AUD",
            "zh": "AUD"
          },
          "symbol": "aud",
          "type": "CURRENCY_FIAT"
        },
        "id": "5d259a54ef5fd91402d7ded4",
        "status": "DEPOSIT_STATUS_CANCELED",
        "type": "TRANSACTION_FIAT"
      },
      {
        "accountId": "5cac769747e672679f87832e",
        "amount": "1000",
        "createdAt": "2019-07-04T07:55:16.400Z",
        "currency": {
          "decimal": 10,
          "id": "5c7c67de21afc513a54c7783",
          "logo": "https://cdn.sdce.com.au/images/icon-aud.png",
          "name": {
            "en": "AUD",
            "zh": "AUD"
          },
          "symbol": "aud",
          "type": "CURRENCY_FIAT"
        },
        "id": "5d1db0e4d5f786f567789494",
        "status": "DEPOSIT_STATUS_SUBMITTED",
        "type": "TRANSACTION_FIAT"
      },
      ...
    ],
    "totalCount": 17
  }
  ```

---

### 查询用户OTC交易订单

`GET` /exchange/otc/order

- Description: 查询用户的OTC订单交易记录
- 头部信息 header :
  - Content-Type: application/json
  - Authorization: Bearer {accessToken}
- 查询参数 query parameters :

  - userId: member ID
  - statusArray: 筛选状态
  - pageIdx: 第几页，从 0 开始
  - pageSize: 每页数据条数
  - side
  - baseSymbol
  - quoteSymbol

- Sample request :

  ```
  https://dev-api.sdce.com.au/exchange/otc/order?userId=5c98250fe60da8cb0eb5cba3&pageSize=10&pageIdx=0
  ```

- Sample response:
  ```json
  {
    "orders": [
      {
        "chatroom_id": "f04ddff1-e34e-40c8-808e-d0588e8507db",
        "events": null,
        "fee": "0.0000002",
        "id": "5e27f6ff518c9f51c1d513ce",
        "instrument": {
          "base": {
            "decimal": 8,
            "id": "5c7c67dd21afc513a54c7782",
            "logo": "https://upload.wikimedia.org/wikipedia/commons/9/9a/BTC_Logo.svg",
            "name": {
              "en": "BTC",
              "zh": "BTC"
            },
            "symbol": "btc",
            "type": "CURRENCY_BTC"
          },
          "code": "btccny",
          "id": "5cb3c93921e6d05d4357f125",
          "name": "BTC CNY",
          "quote": {
            "decimal": 2,
            "id": "5cb191c845d26492c0c28b21",
            "logo": "https://cdn3.iconfinder.com/data/icons/currency-2/460/Yuan-512.png",
            "name": {
              "en": "CNY",
              "zh": "CNY"
            },
            "symbol": "cny",
            "type": "CURRENCY_FIAT"
          }
        },
        "last_updated_time": "2020-01-22T07:32:43.093Z",
        "member_id": "5c98250fe60da8cb0eb5cba3",
        "price": 10000,
        "quote_owner": "5d145c387d797b855d7be9f2",
        "released_time": "1970-01-01T00:00:00.000Z",
        "side": "BID",
        "status": "EXPIRED",
        "time": "2020-01-22T07:17:19.889Z",
        "value": "1",
        "volume": "0.0001"
      },
      {
        "chatroom_id": "bd4219a9-c963-4f9f-b9d4-56bfcaf0a79a",
        "events": null,
        "fee": "0.00300601",
        "id": "5dd7326f1511efb1bda71c8e",
        "instrument": {
          "base": {
            "decimal": 8,
            "id": "5c7c67dd21afc513a54c7782",
            "logo": "https://upload.wikimedia.org/wikipedia/commons/9/9a/BTC_Logo.svg",
            "name": {
              "en": "BTC",
              "zh": "BTC"
            },
            "symbol": "btc",
            "type": "CURRENCY_BTC"
          },
          "code": "btckrw",
          "id": "5dd72063b697cc2d5585cf55",
          "name": "BTC KRW",
          "quote": {
            "decimal": 10,
            "id": "5dd639754a5b23aacf77d4db",
            "name": {
              "en": "KRW"
            },
            "symbol": "krw",
            "type": "CURRENCY_FIAT"
          }
        },
        "last_updated_time": "2019-11-22T00:57:52.340Z",
        "member_id": "5dd728fa7c9c94a66dedd82f",
        "price": 26666.666666666664,
        "quote_owner": "5c98250fe60da8cb0eb5cba3",
        "released_time": "2019-11-22T00:57:52.340Z",
        "side": "BID",
        "status": "COMPLETED",
        "time": "2019-11-22T00:57:19.496Z",
        "value": "40000",
        "volume": "1.5"
      },
      ...
    ],
    "totalCount": 9
  }
  ```

---

### 获取用户OTC订单详情

`GET` /exchange/otc/order/{orderId}

- Description: 查询用户的OTC订单交易记录
- 头部信息 header :
  - Content-Type: application/json
  - Authorization: Bearer {accessToken}
- 路径参数 path parameters:

  - orderId: order ID

- Sample request :

  ```
  https://dev-api.sdce.com.au/exchange/otc/order/5dd7326f1511efb1bda71c8e
  ```

- Sample response:
  ```json
  {
    "orderMemberInfo": {
      "bankPaymentDetail": {
        "accountNumber": "0987654345678",
        "bankBranchInfo": "交通银行",
        "firstName": "Feihong",
        "lastName": "Huang"
      },
      "nickName": "KRW player"
    },
    "otcOrder": {
      "chatroom_id": "bd4219a9-c963-4f9f-b9d4-56bfcaf0a79a",
      "events": null,
      "fee": "0.00300601",
      "id": "5dd7326f1511efb1bda71c8e",
      "instrument": {
        "base": {
          "decimal": 8,
          "id": "5c7c67dd21afc513a54c7782",
          "logo": "https://upload.wikimedia.org/wikipedia/commons/9/9a/BTC_Logo.svg",
          "name": {
            "en": "BTC",
            "zh": "BTC"
          },
          "symbol": "btc",
          "type": "CURRENCY_BTC"
        },
        "code": "btckrw",
        "id": "5dd72063b697cc2d5585cf55",
        "name": "BTC KRW",
        "quote": {
          "decimal": 10,
          "id": "5dd639754a5b23aacf77d4db",
          "name": {
            "en": "KRW"
          },
          "symbol": "krw",
          "type": "CURRENCY_FIAT"
        }
      },
      "last_updated_time": "2019-11-22T00:57:52.340Z",
      "member_id": "5dd728fa7c9c94a66dedd82f",
      "price": 26666.666666666664,
      "quote_owner": "5c98250fe60da8cb0eb5cba3",
      "released_time": "2019-11-22T00:57:52.340Z",
      "side": "BID",
      "status": "COMPLETED",
      "time": "2019-11-22T00:57:19.496Z",
      "value": "40000",
      "volume": "1.5"
    },
    "quoteMemberInfo": {
      "alipayDetail": {
        "QRCode": "/otc/c265e19e-51f2-4db3-b6ef-ad1ca2b9367a_0po3o52qnps0470onp3nrq57oqrso84q.jpg",
        "accountNumber": "支付宝85742396",
        "name": "庄晓曼"
      },
      "bankPaymentDetail": {
        "accountNumber": "6230580000083608078",
        "bankBranchInfo": "学院路支行",
        "firstName": "途",
        "lastName": "肖"
      },
      "nickName": "庄晓曼",
      "wechatPaymentDetail": {
        "QRCode": "/otc/e9cacd37-9429-49a8-ac66-8390bde4d2da_wechatpay.png",
        "accountNumber": "晓曼的微信"
      }
    },
    "quoteRef": {
      "accepted_payment_methods": ["BANK"],
      "events": [
        {
          "price": 2666666.6666666665,
          "time": "2019-11-22T00:56:15.956Z",
          "type": "CREATE_ORDER",
          "value": "400000000000000",
          "volume": "150300601"
        },
        {
          "time": "2019-11-22T00:57:19.509Z",
          "type": "UPDATE_ORDER",
          "value": "0",
          "volume": "0"
        },
        {
          "time": "2019-11-22T00:57:52.337Z",
          "type": "UPDATE_ORDER",
          "value": "0",
          "volume": "0"
        }
      ],
      "id": "5dd7322f1511efb1bda71c8c",
      "instrument": {
        "base": {
          "decimal": 8,
          "id": "5c7c67dd21afc513a54c7782",
          "logo": "https://upload.wikimedia.org/wikipedia/commons/9/9a/BTC_Logo.svg",
          "name": {
            "en": "BTC",
            "zh": "BTC"
          },
          "symbol": "btc",
          "type": "CURRENCY_BTC"
        },
        "code": "btckrw",
        "id": "5dd72063b697cc2d5585cf55",
        "name": "BTC KRW",
        "quote": {
          "decimal": 10,
          "id": "5dd639754a5b23aacf77d4db",
          "name": {
            "en": "KRW"
          },
          "symbol": "krw",
          "type": "CURRENCY_FIAT"
        }
      },
      "last_updated_time": "2019-11-22T00:56:15.940Z",
      "lockedFee": "0",
      "max_value": "40000",
      "min_value": "40000",
      "order_live_time": 900,
      "owner": "5c98250fe60da8cb0eb5cba3",
      "owner_otc_detail": {
        "alipayDetail": {
          "QRCode": "/otc/c265e19e-51f2-4db3-b6ef-ad1ca2b9367a_0po3o52qnps0470onp3nrq57oqrso84q.jpg",
          "accountNumber": "支付宝85742396",
          "name": "庄晓曼"
        },
        "bankPaymentDetail": {
          "accountNumber": "6230580000083608078",
          "bankBranchInfo": "学院路支行",
          "firstName": "途",
          "lastName": "肖"
        },
        "nickName": "庄晓曼",
        "wechatPaymentDetail": {
          "QRCode": "/otc/e9cacd37-9429-49a8-ac66-8390bde4d2da_wechatpay.png",
          "accountNumber": "晓曼的微信"
        }
      },
      "price": 26666.666666666664,
      "processing_volume": "0",
      "side": "ASK",
      "status": "OFF",
      "time": "1970-01-01T00:00:00.000Z",
      "type": "WHOLESALE",
      "value": "0",
      "volume": "0"
    }
  }
  ```

---

### 查询存入历史

`GET` /exchange/member/{memberId}/deposits

- Description: 查询用户所有的存入/转入历史记录
- 头部信息 header :
  - Content-Type: application/json
  - Authorization: Bearer {accessToken}
- 路径参数 path parameters :
  - memberId: member ID
- 查询参数 query parameters :

  - pageIndex: 第几页，从 0 开始
  - pageSize: 每页数据条数
  - accountId: 指定某一个 account 的 accountID

- Sample request :

  ```
  https://dev-api.sdce.com.au/exchange/member/5c98250fe60da8cb0eb5cba3/deposits?pageIndex=0&pageSize=10
  ```

- Sample response:
  ```json
  {
    "result": [
      {
        "accountId": "5cac769747e672679f87832e",
        "amount": "10000",
        "createdAt": "2019-07-10T07:57:08.537Z",
        "currency": {
          "decimal": 10,
          "id": "5c7c67de21afc513a54c7783",
          "logo": "https://cdn.sdce.com.au/images/icon-aud.png",
          "name": {
            "en": "AUD",
            "zh": "AUD"
          },
          "symbol": "aud",
          "type": "CURRENCY_FIAT"
        },
        "id": "5d259a54ef5fd91402d7ded4",
        "status": "DEPOSIT_STATUS_CANCELED",
        "type": "TRANSACTION_FIAT"
      },
      {
        "accountId": "5cac769747e672679f87832e",
        "amount": "1000",
        "createdAt": "2019-07-04T07:55:16.400Z",
        "currency": {
          "decimal": 10,
          "id": "5c7c67de21afc513a54c7783",
          "logo": "https://cdn.sdce.com.au/images/icon-aud.png",
          "name": {
            "en": "AUD",
            "zh": "AUD"
          },
          "symbol": "aud",
          "type": "CURRENCY_FIAT"
        },
        "id": "5d1db0e4d5f786f567789494",
        "status": "DEPOSIT_STATUS_SUBMITTED",
        "type": "TRANSACTION_FIAT"
      },
      ...
    ],
    "totalCount": 17
  }
  ```

---

### 查询取出历史

`GET` /exchange/member/{memberId}/withdraws

- Description: 查询用户所有的取出/转出历史记录
- 头部信息 header :
  - Content-Type: application/json
  - Authorization: Bearer {accessToken}
- 路径参数 path parameters :
  - memberId: member ID
- 查询参数 query parameters :

  - pageIndex: 第几页，从 0 开始
  - pageSize: 每页数据条数
  - accountId: 指定某一个 account 的 accountID

- Sample request :

  ```
  https://dev-api.sdce.com.au/exchange/member/5c98250fe60da8cb0eb5cba3/withdraws?pageIndex=0&pageSize=10
  ```

- Sample response:
  ```json
  {
    "result": [
      {
        "accountId": "5cac769747e672679f87832e",
        "amount": "1000",
        "createdAt": "2019-11-26T01:28:34.846Z",
        "currency": {
          "decimal": 10,
          "id": "5c7c67de21afc513a54c7783",
          "logo": "https://cdn.sdce.com.au/images/icon-aud.png",
          "name": {
            "en": "AUD",
            "zh": "AUD"
          },
          "symbol": "aud",
          "type": "CURRENCY_FIAT"
        },
        "id": "5ddc7fc27c9c94a66dee8bda",
        "memberId": "5c98250fe60da8cb0eb5cba3",
        "status": "WITHDRAW_STATUS_SUBMITTED",
        "targetBank": {
          "accountId": "5cac769747e672679f87832e",
          "bankId": "5d89c0fc5c87243caf80bda7",
          "detail": "{\"area\":\"au\",\"label\":\"我的AUS银行卡\",\"accountName\":\"xiaoman\",\"bankName\":\"CBA\",\"bsb\":\"066888\",\"accountNumber\":\"78901234\",\"street\":\"56\",\"city\":\"Sydney\",\"state\":\"NSW\",\"postcode\":\"2000\"}",
          "isDefault": true,
          "type": "au_aud"
        },
        "type": "TRANSACTION_FIAT",
        "withdrawFee": "0"
      },
      {
        "accountId": "5cac769747e672679f87832e",
        "amount": "1000",
        "createdAt": "2019-11-26T00:53:53.790Z",
        "currency": {
          "decimal": 10,
          "id": "5c7c67de21afc513a54c7783",
          "logo": "https://cdn.sdce.com.au/images/icon-aud.png",
          "name": {
            "en": "AUD",
            "zh": "AUD"
          },
          "symbol": "aud",
          "type": "CURRENCY_FIAT"
        },
        "id": "5ddc77a17c9c94a66dee8bd7",
        "memberId": "5c98250fe60da8cb0eb5cba3",
        "status": "WITHDRAW_STATUS_SUBMITTED",
        "targetBank": {
          "accountId": "5cac769747e672679f87832e",
          "bankId": "5db8c2bb16ad870a022693fa",
          "detail": "{\"area\":\"us/sgp/hk/ca\",\"label\":\"新加坡账户\",\"accountName\":\"xiaoman\",\"bankName\":\"HSBC\",\"accountNumber\":\"1234556\",\"street\":\"16 Huqiao Road\",\"city\":\"Hanzhong\",\"state\":\"SHAANXI SHENG\",\"postcode\":\"723000\",\"swift\":\"BA432TC\"}",
          "type": "global_aud"
        },
        "type": "TRANSACTION_FIAT",
        "withdrawFee": "0"
      },
      ...
    ],
    "totalCount": 78
  }
  ```

---

### 查询订单历史

`GET` /exchange/orders

- Description: 查询用户所有的交易订单历史
- 头部信息 header :
  - Content-Type: application/json
  - Authorization: Bearer {accessToken}
- 查询参数 query parameters :

  - member: member ID
  - pageIndex: 第几页，从 0 开始
  - pageSize: 每页数据条数
  - onlyOpen: true/false 是否是开放（open）订单

- Sample request :

  ```
  https://dev-api.sdce.com.au/exchange/orders?member=5c98250fe60da8cb0eb5cba3&pageIndex=0&pageSize=10
  ```

- Sample response:
  ```json
  {
    "result": [
      {
        "events": null,
        "filled_value": "0",
        "filled_volume": "0",
        "id": "5e019bac9b4891a9112d0df8",
        "instrument": {
          "base": {
            "decimal": 8,
            "id": "5c7c67dd21afc513a54c7782",
            "logo": "https://upload.wikimedia.org/wikipedia/commons/9/9a/BTC_Logo.svg",
            "name": {
              "en": "BTC",
              "zh": "BTC"
            },
            "symbol": "btc",
            "type": "CURRENCY_BTC"
          },
          "code": "btcaud",
          "id": "5c7c67de21027d7dc19596a8",
          "name": "BTC AUD",
          "quote": {
            "decimal": 10,
            "id": "5c7c67de21afc513a54c7783",
            "logo": "https://cdn.sdce.com.au/images/icon-aud.png",
            "name": {
              "en": "AUD",
              "zh": "AUD"
            },
            "symbol": "aud",
            "type": "CURRENCY_FIAT"
          }
        },
        "owner": {
          "clientId": "5c98250fe60da8cb0eb5cba3"
        },
        "price": 5,
        "side": "BID",
        "status": "CANCELLED",
        "time": "2019-12-24T05:01:32.572Z",
        "type": "LIMIT",
        "value": "0",
        "volume": "0"
      },
      {
        "events": null,
        "filled_value": "0",
        "filled_volume": "0",
        "id": "5e018b3c9b4891a9112d0df4",
        "instrument": {
          "base": {
            "decimal": 8,
            "id": "5c7c67dd21afc513a54c7782",
            "logo": "https://upload.wikimedia.org/wikipedia/commons/9/9a/BTC_Logo.svg",
            "name": {
              "en": "BTC",
              "zh": "BTC"
            },
            "symbol": "btc",
            "type": "CURRENCY_BTC"
          },
          "code": "btcaud",
          "id": "5c7c67de21027d7dc19596a8",
          "name": "BTC AUD",
          "quote": {
            "decimal": 10,
            "id": "5c7c67de21afc513a54c7783",
            "logo": "https://cdn.sdce.com.au/images/icon-aud.png",
            "name": {
              "en": "AUD",
              "zh": "AUD"
            },
            "symbol": "aud",
            "type": "CURRENCY_FIAT"
          }
        },
        "owner": {
          "clientId": "5c98250fe60da8cb0eb5cba3"
        },
        "price": 5,
        "side": "BID",
        "status": "CANCELLED",
        "time": "2019-12-24T03:51:24.300Z",
        "type": "LIMIT",
        "value": "0",
        "volume": "0"
      },
      ...
    ],
    "totalCount": 46
  }
  ```

---

### 查询交易历史

`GET` /exchange/instrument/trades

- Description: 查询用户所有的交易历史
- 头部信息 header :
  - Content-Type: application/json
  - Authorization: Bearer {accessToken}
- 查询参数 query parameters :

  - memberId: 用户的 Member ID
  - pageIndex: 第几页，从 0 开始
  - pageSize: 每页数据条数

- Sample request :

  ```
  https://dev-api.sdce.com.au/exchange/instrument/trades?memberId=5c98250fe60da8cb0eb5cba3&pageIndex=0&pageSize=10
  ```

- Sample response:
  ```json
  {
    "result": [
      {
        "askRef": {
          "id": "5d4cd948462fec1956a514bd",
          "owner": {
            "clientId": "5c94550ea6e81417879c37ea"
          },
          "side": "ASK",
          "time": "2019-08-09T02:24:08.957Z",
          "type": "LIMIT"
        },
        "bidRef": {
          "id": "5d427961a7a5860afc955bcc",
          "owner": {
            "clientId": "5c98250fe60da8cb0eb5cba3"
          },
          "side": "BID",
          "time": "2019-08-01T05:32:17.269Z",
          "type": "LIMIT"
        },
        "flag": "SELL_INITIATED",
        "id": "5d4cd949d22bc7a3efbdd843",
        "instrument_ref": {
          "base": {
            "decimal": 8,
            "id": "5c7c67dd21afc513a54c7782",
            "logo": "https://upload.wikimedia.org/wikipedia/commons/9/9a/BTC_Logo.svg",
            "name": {
              "en": "BTC",
              "zh": "BTC"
            },
            "symbol": "btc",
            "type": "CURRENCY_BTC"
          },
          "code": "btcaud",
          "id": "5c7c67de21027d7dc19596a8",
          "name": "BTC AUD",
          "quote": {
            "decimal": 10,
            "id": "5c7c67de21afc513a54c7783",
            "logo": "https://cdn.sdce.com.au/images/icon-aud.png",
            "name": {
              "en": "AUD",
              "zh": "AUD"
            },
            "symbol": "aud",
            "type": "CURRENCY_FIAT"
          }
        },
        "price": 0.26485299999089734,
        "time": "2020-01-19T16:40:45.697Z",
        "value": "0.00000009",
        "volume": "0.00000034"
      },
      ...
    ],
    "totalCount": 100
  }
  ```

---

### 获取OTC交易对

- Description: 获取OTC支持的交易币对，使用[查找交易对信息](#查找交易对信息)，查询参数使用 type=OTC&status=ACTIVE

---

### 查找货币信息

`GET` /exchange/currency

- Description: 获取交易所支持的所有货币数据，或根据条件查找
- 头部信息 header :
  - Content-Type: application/json
  - Authorization: Bearer {accessToken}
- 查询参数 query parameters :

  - symbol: e.g. btc
  - name: e.g. BTC
  - statusArray:
    - 可选值：（可多选，e.g. statusArray=PRESALE,ACTIVE）
      - PRESALE
      - ACTIVE
      - DELISTED
      - SUSPENDED

- Sample request
  ```
  https://api.aho8.com/exchange/currency?statusArray=PRESALE,ACTIVE
  ```
- 返回数据: 所有的货币数据（法币 and 数字货币）
- Sample response:

  ```json
  [
    {
      "decimal": 18,
      "id": "5c947d3eda0d2c6c16ef7a69",
      "logo": "https://icons.sdce.com.au/icon-eth.png",
      "name": {
        "en": "ETH",
        "zh": "ETH"
      },
      "status": "PRESALE",
      "symbol": "eth",
      "type": "CURRENCY_ETH"
    },
    {
      "decimal": 10,
      "id": "5c947d3eda0d2c6c16ef7aea",
      "logo": "https://icons.sdce.com.au/icon-usd.png",
      "name": {
        "en": "USD",
        "zh": "USD"
      },
      "status": "PRESALE",
      "symbol": "usd",
      "type": "CURRENCY_FIAT"
    },
    {
      "contractAddress": "0xdac17f958d2ee523a2206206994597c13d831ec7",
      "decimal": 8,
      "id": "5c947d3eda0d2c6c16ef7aeb",
      "logo": "https://icons.sdce.com.au/icon-usdt.png",
      "name": {
        "en": "USDT",
        "zh": "USDT"
      },
      "status": "PRESALE",
      "symbol": "usdt",
      "type": "CURRENCY_ERC20"
    },
    ...
  ]
  ```

---

### 查找交易对信息

`GET` /exchange/instrument

- Description: 查找(所有)交易对信息 比如 btcaud, ethaud

- 查询参数 query parameters :

  - userInput (选填)： 交易对的代码，如 btcaud, oraeth
  - type (选填): 交易对的类型.
    - available values:
      - MARKET
      - OTC
      - OTC_MARKET
      - FX
  - base(选填)： 被交易货币， 例如 btc, eth
  - quote(选填)：交易基准币，如 aud, btc, eth
  - status(选填)：
    - available values:
      - ACTIVE
      - DELISTED
      - SUSPENDED

- Sample request:

  ```
  https://dev-api.sdce.com.au/exchange/instrument?quote=aud&status=ACTIVE&type=MARKET
  ```

- 返回数据：符合查询条件的所有币对的数据。其中 ticker 字段为币对最新价格与交易数据:
  - close: 收盘价
  - high: 最高价
  - low: 最低价
  - open: 开盘价
  - time: 时间
  - volume: 交易量
- Sample response:

  ```json
  {
    "Result": [
      {
        "ask_fee": {
          "currency": "5c7c67de21afc513a54c7783",
          "decimal": 2
        },
        "base": {
          "decimal": 18,
          "id": "5c7c67dd21afc513a54c7781",
          "logo": "https://upload.wikimedia.org/wikipedia/commons/7/70/Ethereum_logo.svg",
          "name": {
            "en": "ETH",
            "zh": "ETH"
          },
          "symbol": "eth",
          "type": "CURRENCY_ETH"
        },
        "bid_fee": {
          "currency": "5c7c67de21afc513a54c7783",
          "decimal": 10
        },
        "code": "ethaud",
        "id": "5c7c67de21027d7dc19596a9", // instrument id, e.g. in this case, '5c7c67de21027d7dc19596a9' is the id of instrument 'ethaud'
        "name": "ETH AUD",
        "quote": {
          "decimal": 10,
          "id": "5c7c67de21afc513a54c7783",
          "logo": "https://cdn.sdce.com.au/images/icon-aud.png",
          "name": {
            "en": "AUD",
            "zh": "AUD"
          },
          "symbol": "aud",
          "type": "CURRENCY_FIAT"
        },
        "ticker": {
          "close": 30,
          "high": 30,
          "low": 30,
          "open": 30,
          "time": "2019-12-30T06:30:00.000Z",
          "volume": 1440
        },
        "type": "MARKET"
      },
      {
        "ask_fee": {
          "currency": "5c7c67de21afc513a54c7783",
          "decimal": 2
        },
        "base": {
          "decimal": 8,
          "id": "5c7c67dd21afc513a54c7782",
          "logo": "https://upload.wikimedia.org/wikipedia/commons/9/9a/BTC_Logo.svg",
          "name": {
            "en": "BTC",
            "zh": "BTC"
          },
          "symbol": "btc",
          "type": "CURRENCY_BTC"
        },
        "bid_fee": {
          "currency": "5c7c67de21afc513a54c7783",
          "decimal": 2
        },
        "code": "btcaud",
        "id": "5c7c67de21027d7dc19596a8",
        "name": "BTC AUD",
        "quote": {
          "decimal": 10,
          "id": "5c7c67de21afc513a54c7783",
          "logo": "https://cdn.sdce.com.au/images/icon-aud.png",
          "name": {
            "en": "AUD",
            "zh": "AUD"
          },
          "symbol": "aud",
          "type": "CURRENCY_FIAT"
        },
        "ticker": {
          "close": 30000,
          "high": 1999991,
          "low": 0.2648529999908974,
          "open": 2,
          "time": "2019-12-30T06:30:00.000Z",
          "volume": 51241.924554204
        },
        "type": "OTC_MARKET"
      }
    ]
  }
  ```

---

### 交易对kline

`GET` ​/exchange​/instrument​/{instrumentId}​/kline

- Description: 获取交易对 K 线信息

- 查询参数 query parameters :

  - resolution (选填)： 分辨率/精度
    - available values: (D means day, W means week, no unit means minute)
      - 1
      - 5
      - 15
      - 30
      - 60
      - 240
      - 1D
      - 5D
      - 1W
  - startTime (string, 选填): 起始时间
  - endTime (string, 选填): 结束时间

- Sample request:

  ```
  https://api.aho8.com/exchange/instrument/5c947d44da0d2c6c16ef7aed/kline?resolution=1D&startTime=2019-01-12T10%3A59%3A31%2B11%3A00&endTime=2020-01-07T11%3A00%3A31%2B11%3A00
  ```

- Sample response:
  ```json
  {
    "klineBars": [
      {
        "close": 5695.66,
        "high": 5795.95,
        "low": 5409.51,
        "open": 5410.96,
        "time": "2019-04-11T00:00:00.000Z",
        "volume": 5097.437052355999
      },
      {
        "close": 5644.18,
        "high": 5799.99,
        "low": 5400.09,
        "open": 5673.4800000000005,
        "time": "2019-04-12T00:00:00.000Z",
        "volume": 62372.2796476824
      },
      {
        "close": 5726.57,
        "high": 5726.57,
        "low": 5512.74,
        "open": 5690.46,
        "time": "2019-04-13T00:00:00.000Z",
        "volume": 25839.748272999997
      },
      ...
    ]
  }
  ```

---

### 交易对市场深度

`GET` ​​/exchange​/instrument​/{instrumentId}​/depth

- Description: 获取交易对市场深度数据
- 路径参数 path parameters :

  - instrumentId: 币对 ID

- Sample request:

  ```
  https://api.aho8.com/exchange/instrument/5c947d44da0d2c6c16ef7aed/depth
  ```

- Sample response:
  ```json
  {
    "depth": {
      "asks": [],
      "bids": [
        {
          "price": 11229.653255062707,
          "volume": 0.0000068058
        },
        {
          "price": 11251.817027512181,
          "volume": 0.0000144348
        },
        {
          "price": 11060.44222737819,
          "volume": 0.0000424512
        },
        {
          "price": 11073.901886179103,
          "volume": 0.0000325089
        },
        {
          "price": 11100.837136066984,
          "volume": 0.0000309333
        }
      ]
    }
  }
  ```

---

### 查找交易对订单

`GET` ​​/exchange/instrument/orders

- Description: 查找指定交易对的订单数据
- 查询参数 query parameters :

  - instrumentId: 币对 ID
  - pageIndex: 第几页，从 0 开始
  - pageSize: 每页数据条数
  - onlyOpen: 是否是开放（open）订单
  - orderSide: 买入或卖出
    - 可选值:
      - ASK
      - BID

- Sample request:

  ```
  https://api.aho8.com/exchange/instrument/orders?instrumentId=5c947d44da0d2c6c16ef7aed&orderSide=BID&pageIndex=0&pageSize=50&onlyOpen=true
  ```

- Sample response:
  ```json
  {
    "result": [
      {
        "events": null,
        "filled_value": "0",
        "filled_volume": "0",
        "id": "5e13d1265bc2958d1fb9817c",
        "instrument": {
          "base": {
            "decimal": 8,
            "id": "5c947d3eda0d2c6c16ef7a68",
            "logo": "https://icons-exchange.oss-ap-southeast-2.aliyuncs.com/icon-btc.png",
            "name": {
              "en": "BTC",
              "zh": "BTC"
            },
            "symbol": "btc",
            "type": "CURRENCY_BTC"
          },
          "code": "btcaud",
          "id": "5c947d44da0d2c6c16ef7aed",
          "name": "BTC AUD",
          "quote": {
            "decimal": 10,
            "id": "5c947d3eda0d2c6c16ef7a67",
            "logo": "https://icons-exchange.oss-ap-southeast-2.aliyuncs.com/icon-aud.png",
            "name": {
              "en": "AUD"
            },
            "symbol": "aud",
            "type": "CURRENCY_FIAT"
          }
        },
        "owner": {
          "clientId": "5c947d3eda0d2c6c16ef7982"
        },
        "price": 11411.066451572315,
        "side": "BID",
        "status": "OPEN",
        "time": "2020-01-07T00:30:30.134Z",
        "type": "LIMIT",
        "value": "12.69697953",
        "volume": "0.00111269"
      },
      {
        "events": null,
        "filled_value": "15.3570280352",
        "filled_volume": "0.00136485",
        "id": "5e13c9715bc2958d1fb97aa2",
        "instrument": {
          "base": {
            "decimal": 8,
            "id": "5c947d3eda0d2c6c16ef7a68",
            "logo": "https://icons-exchange.oss-ap-southeast-2.aliyuncs.com/icon-btc.png",
            "name": {
              "en": "BTC",
              "zh": "BTC"
            },
            "symbol": "btc",
            "type": "CURRENCY_BTC"
          },
          "code": "btcaud",
          "id": "5c947d44da0d2c6c16ef7aed",
          "name": "BTC AUD",
          "quote": {
            "decimal": 10,
            "id": "5c947d3eda0d2c6c16ef7a67",
            "logo": "https://icons-exchange.oss-ap-southeast-2.aliyuncs.com/icon-aud.png",
            "name": {
              "en": "AUD"
            },
            "symbol": "aud",
            "type": "CURRENCY_FIAT"
          }
        },
        "owner": {
          "clientId": "5c947d3eda0d2c6c16ef7982"
        },
        "price": 11251.817027512181,
        "side": "BID",
        "status": "PARTIAL_FILLED",
        "time": "2020-01-06T23:57:37.461Z",
        "type": "LIMIT",
        "value": "15.35704247",
        "volume": "0.00136485"
      },
      ...
    ],
    "totalCount": 7
  }
  ```

---

### 查找交易对已完成交易

`GET` ​​/exchange/instrument/{instrumentId}/trade

- Description: 查找指定交易对的公开已完成交易订单数据
- path parameters :
  - instrumentId: 币对 ID
- 查询参数 query parameters :

  - pageIndex: 第几页，从 0 开始
  - pageSize: 每页数据条数

- Sample request:

  ```
  // BTC-AUD
  https://api.aho8.com/exchange/instrument/5c947d44da0d2c6c16ef7aed/trade?pageIndex=0&pageSize=50
  ```

- Sample response:
  ```json
  // BTC-AUD
  {
    "result": [
      {
        "askRef": {
          "id": "5e13f6f25bc2958d1fb9a50c",
          "owner": {
            "clientId": "5c947d3eda0d2c6c16ef7982"
          },
          "side": "ASK",
          "time": "2020-01-07T03:11:46.029Z",
          "type": "LIMIT"
        },
        "bidRef": {
          "id": "5e13f7495bc2958d1fb9a542",
          "owner": {
            "clientId": "5c947d3eda0d2c6c16ef7982"
          },
          "side": "BID",
          "time": "2020-01-07T03:13:13.506Z",
          "type": "LIMIT"
        },
        "flag": "BUY_INITIATED",
        "id": "5e13f749299accb93d2a6d6f",
        "instrument_ref": {
          "base": {
            "decimal": 8,
            "id": "5c947d3eda0d2c6c16ef7a68",
            "logo": "https://icons-exchange.oss-ap-southeast-2.aliyuncs.com/icon-btc.png",
            "name": {
              "en": "BTC",
              "zh": "BTC"
            },
            "symbol": "btc",
            "type": "CURRENCY_BTC"
          },
          "code": "btcaud",
          "id": "5c947d44da0d2c6c16ef7aed",
          "name": "BTC AUD",
          "quote": {
            "decimal": 10,
            "id": "5c947d3eda0d2c6c16ef7a67",
            "logo": "https://icons-exchange.oss-ap-southeast-2.aliyuncs.com/icon-aud.png",
            "name": {
              "en": "AUD"
            },
            "symbol": "aud",
            "type": "CURRENCY_FIAT"
          }
        },
        "price": 11629.3046024,
        "time": "2020-01-07T03:13:13.872Z",
        "value": "0.01831923",
        "volume": "0.00000158"
      },
      {
        "askRef": {
          "id": "5e13f6495bc2958d1fb9a463",
          "owner": {
            "clientId": "5c947d3eda0d2c6c16ef7982"
          },
          "side": "ASK",
          "time": "2020-01-07T03:08:57.265Z",
          "type": "LIMIT"
        },
        "bidRef": {
          "id": "5e13f6c05bc2958d1fb9a4e3",
          "owner": {
            "clientId": "5c947d3eda0d2c6c16ef7982"
          },
          "side": "BID",
          "time": "2020-01-07T03:10:56.072Z",
          "type": "LIMIT"
        },
        "flag": "BUY_INITIATED",
        "id": "5e13f6c0299accb93d2a6d6a",
        "instrument_ref": {
          "base": {
            "decimal": 8,
            "id": "5c947d3eda0d2c6c16ef7a68",
            "logo": "https://icons-exchange.oss-ap-southeast-2.aliyuncs.com/icon-btc.png",
            "name": {
              "en": "BTC",
              "zh": "BTC"
            },
            "symbol": "btc",
            "type": "CURRENCY_BTC"
          },
          "code": "btcaud",
          "id": "5c947d44da0d2c6c16ef7aed",
          "name": "BTC AUD",
          "quote": {
            "decimal": 10,
            "id": "5c947d3eda0d2c6c16ef7a67",
            "logo": "https://icons-exchange.oss-ap-southeast-2.aliyuncs.com/icon-aud.png",
            "name": {
              "en": "AUD"
            },
            "symbol": "aud",
            "type": "CURRENCY_FIAT"
          }
        },
        "price": 11580.83951028,
        "time": "2020-01-07T03:10:56.257Z",
        "value": "0.8668258373",
        "volume": "0.00007485"
      },
      ...
    ],
    "totalCount": 131496
  }
  ```

---

### 创建用户账户

`POST` /exchange/member/{memberId}/account

- Description: 用户自己创建一个对应某一个币种的账户（部分货币账户会在创建用户时自动创建）
- 头部信息 header :
  - Content-Type: application/json
  - Authorization: Bearer {accessToken}
- 路径参数 path parameters :

- Body

  - type: 货币类型，Spot（数字货币）或 Fiat（法币）
  - currency: 币种 currency ID

- Sample request

  ```
  https://dev-api.sdce.com.au/exchange/member/5c98250fe60da8cb0eb5cba3/account

  body:
  { "type":"Spot", "currency":"5d0b2bba79b87055e50bbab2" }
  ```

- Sample response:

  ```json
  {
    "Result": {
      "accountId": "5c99d2929224f13514cb5b8e"
    }
  }
  ```

---

### 查找用户账户信息

`GET` /exchange/member/{memberId}/account

- 头部信息 header :
  - Content-Type: application/json
  - Authorization: Bearer {accessToken}
- 路径参数 path parameters :
  - memberId(必填): member ID
- 查询参数 query parameters :

  - pageIndex(必填): 第几页，从 0 开始
  - pageSize(必填): 每页数据条数(可设置大一些以拿回所有币种账户)
  - coinID(选填): 根据货币 ID 筛选
  - statusList(选填，暂时没有用到)

- Sample request

  ```
  https://dev-api.sdce.com.au/exchange/member/5c98250fe60da8cb0eb5cba3/account?pageIndex=0&pageSize=10
  ```

- 返回数据: 用户账户数据，其中数组中是不同货币对应的账户信息，可使用数字货币账户中的 depositAddress 字段来[获取钱包地址](#获取钱包地址)。
- Sample response:

  ```json
  {
    "Result": {
      "account": [
        {
          "Status": "Primary",
          "balance": "1000",
          "bankDetails": [],
          "createdAt": "2019-03-27T01:42:12.285Z",
          "currency": {
            "decimal": 18,
            "id": "5c7c67dd21afc513a54c7781",
            "logo": "https://upload.wikimedia.org/wikipedia/commons/7/70/Ethereum_logo.svg",
            "name": {
              "en": "ETH",
              "zh": "ETH"
            },
            "symbol": "eth",
            "type": "CURRENCY_ETH"
          },
          "depositAddress": "0x92DCe16fA5f035eAd45c11B3A0dBa929B72A77AD",
          "id": "5c9ad4f480b8a4c2db15a944",
          "onHold": "0",
          "owner": "5c98250fe60da8cb0eb5cba3",
          "type": "Spot",
          "updatedAt": "2019-03-27T01:42:12.285Z",
          "walletDetails": [
            {
              "accountId": "5c9ad4f480b8a4c2db15a944",
              "address": "0x59749b0D34BaC5603B0E592cfdA665Da2c62dfea",
              "isDefault": true,
              "label": "Shawn",
              "walletId": "5c9c61e9b7aac39cce876516"
            }
          ]
        },
        ...
      ]
    }
  }
  ```

---

### 生成钱包地址

`POST` /exchange/member/{memberId}/account/{accountId}/depositAddr

- 头部信息 header :
  - Content-Type: application/json
  - Authorization: Bearer {accessToken}
- 路径参数 path parameters :

  - memberId(必填): member ID
  - accountId(必填): account ID

- Sample request

  ```
  https://dev-api.sdce.com.au/exchange/member/5c98250fe60da8cb0eb5cba3/account/5d54c1706499776bab2f2c03/depositAddr
  ```

- 返回数据: 账户钱包地址
- Sample response:

  ```json
  {
    "depositAddress": "2Mux9kbytbBmuTg42GDVohmS26sAq4o9USL"
  }
  ```

---

### 获取钱包地址

- Description: 各个货币钱包地址在[查找用户账户信息](#查找用户账户信息)的数据中，每种货币对应账户的 depositAddress 字段中获取。

---

### 存入澳币 Poli 或转账

`POST` /exchange/member/{memberId}/account/{accountId}/depositFiat

- 头部信息 header :
  - Content-Type: application/json
  - Authorization: Bearer {accessToken}
- 路径参数 path parameters :

- Body
  - accountId: 账户 ID
  - value: 存款数量

Sample request

```
https://dev-api.sdce.com.au/exchange/member/5c9c61e9b7aac39cce87abcd/account/5c9c61e9b7aac39cce875678/depositFiat

body:
{
    "accountID": "5c9c61e9b7aac39cce87abcd",
    "value": "100"
}
```

返回数据:

- Result: 存款事件 ID
- DepositUrl: Poli callback Url

```json
{
  "Result": "5c9c61e9b7aac39cce87aaaa",
  "fieldMask": "someurl"
}
```

---

### 存入记录

`GET` /exchange/member/{memberId}/deposits

- 查询参数 query parameters：
  - accountId: string // AUD account ID
  - pageIndex： integer// 返回存款历史列表的页数的 index
  - pageSize: integer //每页包含多少存款历史

返回结果：

```
{
    "result": [
        {
            "accountId": "5cac769747e672679f87832e",
            "amount": "1000",
            "createdAt": "2019-06-05T06:10:27.656Z",
            "currency": {
                "decimal": 10,
                "id": "5c7c67de21afc513a54c7783",
                "logo": "https://cdn.sdce.com.au/images/icon-aud.png",
                "name": {
                    "en": "AUD",
                    "zh": "AUD"
                },
                "symbol": "aud",
                "type": "CURRENCY_FIAT"
            },
            "id": "5cf75cd3ae1d611a1addd4f7",
            "status": "DEPOSIT_STATUS_SUBMITTED",  //存款事件状态
            "type": "TRANSACTION_FIAT"
        },
        {
            "accountId": "5cac769747e672679f87832e",
            "amount": "20",
            "createdAt": "2019-05-13T07:20:59.739Z",
            "currency": {
                "decimal": 10,
                "id": "5c7c67de21afc513a54c7783",
                "logo": "https://cdn.sdce.com.au/images/icon-aud.png",
                "name": {
                    "en": "AUD",
                    "zh": "AUD"
                },
                "symbol": "aud",
                "type": "CURRENCY_FIAT"
            },
            "id": "5cd91adb8a7221303ef2b99b",
            "status": "DEPOSIT_STATUS_SUBMITTED",
            "type": "TRANSACTION_FIAT"
        }
    ],
    "totalCount": 2
}
```

---

### 取出记录

GET /exchange/member/{memberId}/withdraws

查询参数：

- accountId: string // AUD account ID
- pageIndex： integer// 返回取款历史列表的页数的 index
- pageSize: integer //每页包含多少取款历史

返回结果：

```
{
    "result": [
        {
            "accountId": "5cac769747e672679f87832e",
            "amount": "120",
            "createdAt": "2019-05-21T00:09:08.430Z",
            "currency": {
                "decimal": 10,
                "id": "5c7c67de21afc513a54c7783",
                "logo": "https://cdn.sdce.com.au/images/icon-aud.png",
                "name": {
                    "en": "AUD",
                    "zh": "AUD"
                },
                "symbol": "aud",
                "type": "CURRENCY_FIAT"
            },
            "id": "5ce341a4e019938229018cae",
            "memberId": "5c98250fe60da8cb0eb5cba3",
            "status": "WITHDRAW_STATUS_SUBMITTED",
            "targetBank": {
                "accountId": "5cac769747e672679f87832e",
                "accountName": "john",
                "accountNumber": "10665248",
                "bankId": "5cc7f589d78e53b1373869d3",
                "bsb": "062667",
                "isDefault": true
            },
            "type": "TRANSACTION_FIAT",
            "withdrawFee": "0"
        }
    ],
    "totalCount": 2
}
```

---

### 提取法币

`POST` /exchange/member/{memberId}/account/{accountId}/withdrawFiat

- Description: 从 SDCE 系统钱包，提取法币至用户保存的法币取现银行
- 头部信息 header :
  - Content-Type: application/json
  - Authorization: Bearer {accessToken}
- 路径参数 path parameters :
  - memberId(必填): member ID
  - accountId(必填): 取款货币账户的 ID
- Body:
  - withdrawFiatRequest
    - bankId: 选择的取款地址的 ID，参见[获取法币取款地址](#获取法币取款地址)
    - amount: 取现金额
  - verification
    - smsCode: 短信验证码
    - emailCode: 邮件验证码
- Sample request:

  ```json
  https://dev-api.sdce.com.au/exchange/member/5c98250fe60da8cb0eb5cba3/account/5cac769747e672679f87832e/withdrawFiat

  body:
  {
    "withdrawFiatRequest": {
      "bankId": "5e1fa981a019bf8f0c1b10ee",
      "amount": "200"
    },
    "verification": {
      "smsCode": "5234",
      "emailCode": "334745"
    }
  }
  ```

- Sample response:
  ```json
  { "Result": "5e322281de2c6d1655ff9a34" }
  ```

---

### 提取数字货币

`POST` /exchange/member/{memberId}/account/{accountId}/withdrawCoin

- Description: 从 SDCE 系统钱包提取数字货币至用户保存的其他地址
- 头部信息 header :
  - Content-Type: application/json
  - Authorization: Bearer {accessToken}
- 路径参数 path parameters :
  - memberId(必填): member ID
  - accountId(必填): 取款货币账户的 ID
- Body:
  - withdrawCoinRequest
    - walletId: 选择的取款地址的 ID，参见[获取数字货币取款地址](#获取数字货币取款地址)
    - amount: 取现金额
  - verification
    - smsCode: 短信验证码
    - emailCode: 邮件验证码
- Sample request:

  ```json
  https://dev-api.sdce.com.au/exchange/member/5c98250fe60da8cb0eb5cba3/account/5c99d2929224f13514cb5b8e/withdrawCoin

  body:
  {
    "withdrawCoinRequest": {
      "amount": "0.5",
      "walletId": "5e322d3ea019bf8f0c1b10f4"
    },
    "verification": {
      "smsCode": "3574",
      "emailCode": "556048"
    }
  }
  ```

- Sample response:
  ```json
  { "Result": "5e322281de2c6d1655ff9a34" }
  ```

---

### 创建法币取款地址

`POST` /exchange/member/{memberId}/account/{accountId}/withdrawFiat/fundSource

- Description: 创建一个法币的取款账户信息，用于法币收款
- 头部信息 header :
  - Content-Type: application/json
  - Authorization: Bearer {accessToken}
- 路径参数 path parameters :
  - memberId(必填): member ID
  - accountId(必填): 取款货币账户的 ID
- Body:

  - detail(\*发送时需要转为 json string):
    - area: 'au'或'us/sgp/hk/ca'，表示开卡行地区为澳大利亚 或 美国/新加坡/香港/加拿大
    - label: 用户自定的取款账户标识名称
    - accountName: 账户名
    - bankName: 银行名称
    - bsb: bsb 码，仅在 area 为'au'时填写
    - swift: SWIFT 或 BIC 码，仅在 area 为'us/sgp/hk/ca'时填写
    - accountNumber: 账户号码
    - street: 银行地址街道
    - city: 银行所在城市
    - state: 银行所在州/省
    - postcode: 邮编
  - type(string): 可选值如下
    - au_aud(澳大利亚澳币账户)
    - au_usd(澳大利亚美元账户)
    - global_aud(澳大利亚澳币账户)
    - global_usd(澳大利亚美元账户)
  - makeDefault(boolean): true | false，是否为默认取现账户

* Sample request

  ```json
  https://dev-api.sdce.com.au/exchange/member/5c98250fe60da8cb0eb5cba3/account/5cac769747e672679f87832e/withdrawFiat/fundSource

  {
    "detail": "{\"area\":\"au\",\"label\":\"澳大利亚银行卡\",\"accountName\":\"Ziqi Li\",\"bankName\":\"ANZ\",\"bsb\":\"066888\",\"accountNumber\":\"78901234\",\"street\":\"56 Pitt\",\"city\":\"Sydney\",\"state\":\"NSW\",\"postcode\":\"2000\"}",
    "type": "au_aud",
    "makeDefault": true
  }

  or

  {
    "detail": "{\"area\":\"us/sgp/hk/ca\",\"label\":\"Ziqi的香港账户\",\"accountName\":\"Ziqi Li\",\"bankName\":\"HSBC\",\"accountNumber\":\"1234556\",\"street\":\"333 George St\",\"city\":\"Sydney\",\"state\":\"NSW\",\"postcode\":\"2000\",\"swift\":\"BA432TC\"}",
    "type": "global_aud",
    "makeDefault": false
  }
  ```

* 返回数据: 新建账户的 ID

  ```json
  { "id": "5d118a28831df60bc0aaaa5c" }
  ```

---

### 获取法币取款地址

`GET` /exchange/member/{memberId}/account/{accountId}/withdrawFiat/fundSource

- Description: 获取该货币账户内所有已添加的取款地址
  - 也可以通过[查找用户账户信息](#查找用户账户信息)在该币种账户信息中的 bankDetails 字段直接获取
- 头部信息 header :
  - Content-Type: application/json
  - Authorization: Bearer {accessToken}
- 路径参数 path parameters :

  - memberId(必填): member ID
  - accountId(必填): 取款货币账户的 ID

- Sample request

```
https://dev-api.sdce.com.au/exchange/member/5c98250fe60da8cb0eb5cba3/account/5cac769747e672679f87832e/withdrawFiat/fundSource
```

- 返回数据: 用户该法币的取款账户信息
  - accountId: 该法币账户 ID
  - bankId: 法币取款账户 ID
  - detail 字段为 string 形式的 json object
    - area: 'au'或'us/sgp/hk/ca'，表示开卡行地区为澳大利亚 或 美国/新加坡/香港/加拿大
    - label: 用户自定的取款账户标识名称
    - accountName: 账户名
    - bankName: 银行名称
    - bsb: bsb 码，仅在 area 为'au'时存在
    - swift: SWIFT 或 BIC 码，仅在 area 为'us/sgp/hk/ca'时存在
    - accountNumber: 账户号码
    - street: 银行地址街道
    - city: 银行所在城市
    - state: 银行所在州/省
    - postcode: 邮编
  - isDefault: 是否为默认取款账户
  - type(string): 可选值如下
    - au_aud(澳大利亚澳币账户)
    - au_usd(澳大利亚美元账户)
    - global_aud(澳大利亚澳币账户)
    - global_usd(澳大利亚美元账户)
- Sample response:
  ```json
  {
    "Result": [
      {
        "accountId": "5cac769747e672679f87832e",
        "bankId": "5e1fa1aaa019bf8f0c1b10ed",
        "detail": "{\"area\":\"au\",\"label\":\"澳大利亚银行卡1\",\"accountName\":\"Ziqi Li\",\"bankName\":\"ANZ\",\"bsb\":\"066888\",\"accountNumber\":\"78901234\",\"street\":\"56 Pitt\",\"city\":\"Sydney\",\"state\":\"NSW\",\"postcode\":\"2000\"}",
        "isDefault": true,
        "type": "au_aud"
      },
      {
        "accountId": "5cac769747e672679f87832e",
        "bankId": "5e1fa981a019bf8f0c1b10ee",
        "detail": "{\"area\":\"us/sgp/hk/ca\",\"label\":\"Ziqi的香港账户\",\"accountName\":\"Ziqi Li\",\"bankName\":\"HSBC\",\"accountNumber\":\"1234556\",\"street\":\"333 George St\",\"city\":\"Sydney\",\"state\":\"NSW\",\"postcode\":\"2000\",\"swift\":\"BA432TC\"}",
        "type": "global_aud"
      }
    ]
  }
  ```

---

### 更改法币默认取款地址

`PUT` /exchange/member/{memberId}/account/{accountId}/withdrawFiat/fundSource/{fundSourceId}

- 头部信息 header :
  - Content-Type: application/json
  - Authorization: Bearer {accessToken}
- 路径参数 path parameters :
  - memberId(必填): member ID
  - accountId(必填): 取款货币账户的 ID
  - fundSourceId(必填): 对应创建的存款地址 account 中的 bankId
- Body

  - makeDefault(boolean): 是否改为默认

- Sample request

  ```
  https://dev-api.sdce.com.au/exchange/member/5c98250fe60da8cb0eb5cba3/account/5cac769747e672679f87832e/withdrawFiat/fundSource/5e1fa981a019bf8f0c1b10ee

  body:
  {
    "makeDefault": true
  }
  ```

- Sample response:

  ```json
  {
    "success": true
  }
  ```

---

### 删除法币取款地址

`DELETE` /exchange/member/{memberId}/account/{accountId}/withdrawFiat/fundSource/{fundSourceId}

- 头部信息 header :
  - Content-Type: application/json
  - Authorization: Bearer {accessToken}
- 路径参数 path parameters :

  - memberId(必填): member ID
  - accountId(必填): 取款货币账户的 ID
  - fundSourceId(必填): 对应创建的存款地址 account 中的 bankId

- Sample request

  ```
  https://dev-api.sdce.com.au/exchange/member/5c98250fe60da8cb0eb5cba3/account/5cac769747e672679f87832e/withdrawFiat/fundSource/5d89c0fc5c87243caf80bda7
  ```

- Sample response:

  ```json
  {
    "success": true
  }
  ```

---

### 创建数字货币取款地址

`POST` /exchange/member/{memberId}/account/{accountId}/withdrawCoin/fundSource

- Description: 为数字货币账户添加提币地址（收取地址）
- 头部信息 header :
  - Content-Type: application/json
  - Authorization: Bearer {accessToken}
- 路径参数 path parameters :
  - memberId(必填): member ID
  - accountId(必填): 取款货币账户的 ID
- Body

  - address: 数字货币钱包地址
  - label: 显示的标签名称
  - makeDefault(boolean): 是否将新创建的账户作为默认取款账户

- Sample request

  ```
  https://dev-api.sdce.com.au/exchange/member/5c98250fe60da8cb0eb5cba3/account/5c99d2929224f13514cb5b8e/withdrawCoin/fundSource

  body:
  {
    "address":"2N9STNWQkMdNNxDqrH12hmTbVokoy32kUX6",
    "label":"Wayne",
    "makeDefault":true
  }
  ```

- 返回数据: 新建账户的 ID
- Sample response:
  ```json
  { "id": "5d11a130831df60bc0aaaa5f" }
  ```

---

### 获取数字货币取款地址

`GET` /exchange/member/{memberId}/account/{accountId}/withdrawCoin/fundSource

- 头部信息 header :
  - Content-Type: application/json
  - Authorization: Bearer {accessToken}
- 路径参数 path parameters :

  - memberId(必填): member ID
  - accountId(必填): 取款货币账户的 ID

- Sample request

  ```
  https://dev-api.sdce.com.au/exchange/member/5c98250fe60da8cb0eb5cba3/account/5c99d2929224f13514cb5b8e/withdrawCoin/fundSource
  ```

- 返回数据: 用户所有数字货币取款地址数据
- Sample response:

  ```json
  {
    "Result": [
      {
        "accountId": "5c99d2929224f13514cb5b8e",
        "address": "2N9STNWQkMdNNxDqrH12hmTbVokoy32kUX6",
        "isDefault": true,
        "label": "Wayne",
        "walletId": "5d11a130831df60bc0aaaa5f"
      },
      ...
    ]
  }
  ```

---

### 更改数字货币默认取款地址

`PUT` /exchange/member/{memberId}/account/{accountId}/withdrawCoin/fundSource/{FundSourceId}

- 头部信息 header :
  - Content-Type: application/json
  - Authorization: Bearer {accessToken}
- 路径参数 path parameters :
  - memberId(必填): member ID
  - accountId(必填): 取款货币账户的 ID
  - fundSourceId(必填): 对应创建的存款地址 account 中的 walletId
- Body

  - makeDefault(boolean): 是否改为默认

- Sample request:

  ```
  https://dev-api.sdce.com.au/exchange/member/5c98250fe60da8cb0eb5cba3/account/5c99d2929224f13514cb5b8e/withdrawCoin/fundSource/5d0c91faff8703f7519bc5ba

  body:
  {
    "makeDefault": true
  }
  ```

- Sample response:

  ```json
  {
    "success": true
  }
  ```

---

### 提币与取现费用

`GET` /exchange/withdraw/fee

- 头部信息 header :
  - Content-Type: application/json
  - Authorization: Bearer {accessToken}
- 查询参数 query parameters :

  - currencies: 货币名称, e.g. 'btc', 'aud', 'btc,aud'

- Sample request

  ```
  https://dev-api.sdce.com.au/exchange/withdraw/fee?currencies=eth
  ```

- Sample response:

  ```json
  {
    "result": [
      {
        "fee": "0.01", // 取币费用，以该货币自身为单位
        "minimum": "0.05", // 最低取币取现值
        "symbol": "eth" // 货币符号
      }
    ]
  }
  ```

---

### 查询提币与取现限额

`GET` /exchange/member/limits

- 头部信息 header :
  - Content-Type: application/json
  - Authorization: Bearer {accessToken}
- 查询参数 query parameters :

  - kycLevel: member 信息中的 kycLevel 字段值

- Sample request

  ```
  https://dev-api.sdce.com.au/exchange/member/limits?kycLevel=KYC_IDENTITY_VERIFIED
  ```

- Sample response:

  ```json
  {
    "audDepositLimit": "9000",
    "audMinimumWithdraw": "100",
    "audWithdrawLimit": "9000",
    "coinWithdrawLimit": "100",
    "level": "KYC_IDENTITY_VERIFIED"
  }
  ```

---

### 删除数字货币取款地址

`DELETE` /exchange/member/{memberId}/account/{accountId}/withdrawCoin/fundSource/{FundSourceId}

- 头部信息 header :
  - Content-Type: application/json
  - Authorization: Bearer {accessToken}
- 路径参数 path parameters :

  - memberId(必填): member ID
  - accountId(必填): 取款货币账户的 ID
  - fundSourceId(必填): 对应创建的存款地址 account 中的 walletId

- Sample request

  ```
  https://dev-api.sdce.com.au/exchange/member/5c98250fe60da8cb0eb5cba3/account/5c99d2929224f13514cb5b8e/withdrawCoin/fundSource/5d11a130831df60bc0aaaa5f
  ```

- Sample response:

  ```json
  {
    "success": true
  }
  ```

---

### 发送短信验证码

`POST` /exchange/member/sms

- Description: 发送短信验证码至用户绑定的手机，用于用户取币时验证使用
- Sample Request
  ```
  https://api.aho8.com/exchange/member/sms
  ```
- Sample response:
  ```json
  { "Result": "Sucess" }
  ```

---

### 发送邮件验证码

- Description: 发送验证码至用户邮箱，用于用户取币时验证使用
- 使用 Auth0-js 发送，Ref: https://auth0.com/docs/api/authentication#get-code-or-link

  ```js
  const webAuth = new auth0.WebAuth({
    domain: 'sdce.au.auth0.com',
    clientID: 'juxW8MuSfS4AxEA1DgJLO1i7HQsoVBgl',
  });

  webAuth.passwordlessStart(
    {
      connection: 'email',
      send: 'code',
      email, // user email address
    },
    (err, res) => {}
  );
  ```

---

### 获取账户存入记录

`GET` /exchange/member/{memberId}/deposits

- 头部信息 header :
  - Content-Type: application/json
  - Authorization: Bearer {accessToken}
- 路径参数 path parameters :
  - memberId(必填): member ID
  - accountId(必填): 取款货币账户的 ID
- Body
  - address: 数字货币钱包地址
  - label: 显示的标签名称
  - makeDefault(boolean): 是否将新创建的账户作为默认取款账户

Sample request

```
https://dev-api.sdce.com.au/exchange/member/5c98250fe60da8cb0eb5cba3/account/5c99d2929224f13514cb5b8e/withdrawCoin/fundSource

body:
{
  "address":"2N9STNWQkMdNNxDqrH12hmTbVokoy32kUX6",
  "label":"Wayne",
  "makeDefault":true
}
```

返回数据: 新建账户的 ID

```json
{ "id": "5d11a130831df60bc0aaaa5f" }
```

---

### 获取兑换报价信息

`GET` /merchant/quote/sdce

- 头部信息 header :
  - Content-Type: application/json
  - Authorization: Bearer {accessToken}
- 查询参数 query parameters :

  - ticker(必填，string): 汇率查询币对 instrument，可选值:
    - audcny
    - usdtusd
    - usdtcny
    - usdcny

- Sample request

  ```
  https://api.aho8.com/merchant/quote/sdce?ticker=audcny
  ```

- 返回数据: 对应币对 买入(ASK)/卖出(BID) 报价
- Sample response
  ```json
  {
    "quote": {
      "buyPrice": {
        "currency": "cny",
        "price": 4.840665225277333,
        "updatedAt": "2020-01-30T03:46:41.291Z"
      },
      "sellPrice": {
        "currency": "cny",
        "price": 4.638889731173797,
        "updatedAt": "2020-01-30T03:46:41.291Z"
      },
      "ticker": "audcny"
    }
  }
  ```

---

### 创建兑换订单

`POST` /merchant/order

- 头部信息 header :
  - Content-Type: application/json
  - Authorization: Bearer {accessToken}
- Body

  - quantity: 卖出/买入 USD/AUD/USDT 的 symbol 和数量
  - ticker: 币对 instrument 的 symbol
  - counterpartyAction: "BUY"|"SELL"
  - counterpartyRequestID: ID 标识

- Sample request (这个请求为 CNY 兑换 AUD，客户想要得到 200AUD)

  ```json
  https://dev-api.sdce.com.au/merchant/order

  body:
  {
    "quantity": {
      "currency": "aud",
      "quantity": "200"
    },
    "ticker": "audcny",
    "amount": {
      "amount": "1000.00",
      "currency": "cny"
    },
    "counterpartyAction": "BUY", // 1000CNY->200AUD
    "counterpartyRequestID": "sdce-bolt"
  }
  ```

- Sample response:

  ```json
  {
    "amount": {
      "amount": "1000.000000",
      "currency": "cny"
    },
    "counterPartyRequestId": "sdce-bolt",
    "quantity": {
      "currency": "aud",
      "quantity": "175.000000" // 扣除了手续费
    },
    "side": "BUY",
    "tradeId": "5e32538c3ce49593167fc4fd",
    "unitPrice": {
      "currency": null,
      "price": null,
      "updatedAt": "0001-01-01T00:00:00.000Z"
    }
  }
  ```

---

### 创建USDT收付款信息

- 参见 [创建数字货币取款地址](#创建数字货币取款地址)

---

### 创建CNY收付款信息

`POST` /exchange/member/{memberId}/account/{accountId}/withdrawFiat/fundSource

- Description: 创建一个 CNY 的收付款信息
- 头部信息 header :
  - Content-Type: application/json
  - Authorization: Bearer {accessToken}
- 路径参数 path parameters :
  - memberId(必填): member ID
  - accountId(必填): CNY 账户的 ID
- Body:
  - detail(\*发送时需要转为 json string):
    - label: 用户自定的取款账户标识名称
    - name: 姓名
    - bankName: 银行名称
    - bankDetail: 支行信息
    - bankNo: 银行卡号
  - type(string): 'cn_cny'
  - makeDefault(boolean): true | false，是否为默认取现账户
- Sample request

  ```json
  https://dev-api.sdce.com.au/exchange/member/5c98250fe60da8cb0eb5cba3/account/5cb191d6418948fb3385d19a/withdrawFiat/fundSource

  {
    "detail": "{\"label\":\"CNY账户\",\"name\":\"张翔\",\"bankName\":\"大连银行\",\"bankDetail\":\"大连银行香一街支行\",\"bankNo\":\"12345678901234\"}",
    "type": "cn_cny",
    "makeDefault": true
  }
  ```

- Sample response:
  ```json
  { "id": "5e3258a1a019bf8f0c1b10f5" }
  ```

---

### 创建AUD/USD收付款信息

- 参见 [创建法币取款地址](#创建法币取款地址)

---

### 获取兑换订单历史

`GET` /merchant/member/order

- Description: 查询用户兑换订单历史
- 头部信息 header :
  - Content-Type: application/json
  - Authorization: Bearer {accessToken}
- 查询参数 query parameters :
  - pageIndex: 第几页，从 0 开始
  - pageSize: 每页数据条数
- Sample request:
  ```
  https://dev-api.sdce.com.au/merchant/member/order?&pageIndex=1&pageSize=10
  ```
- Sample response:
  ```json
  {
    "result": [
      {
        "amount": null,
        "counterPartyRequestId": "sdce-bolt",
        "createdAt": "2019-10-29T22:40:12.808Z",
        "currencyQuote": {
          "amount": {
            "amount": "2000.000000",
            "currency": "cny"
          },
          "buyUnitPrice": {
            "currency": "cny",
            "price": 4.9,
            "updatedAt": "2019-09-25T02:20:37.545Z"
          },
          "fee": {
            "currency": "aud",
            "value": "25"
          },
          "quantity": {
            "currency": "aud",
            "quantity": "383.163265"
          },
          "sellUnitPrice": {
            "currency": "cny",
            "price": 4.755828011467897,
            "updatedAt": "2019-09-19T01:37:40.790Z"
          }
        },
        "executedAt": "2019-10-29T23:10:15.000Z",
        "extraInfo": "{\"raw\":\"\"}",
        "id": "5db8bfcc93c807ca5e6086c4",
        "mchMargin": {
          "amount": 0,
          "currency": ""
        },
        "merchantId": null,
        "quantity": null,
        "side": "BUY",
        "status": "EXPIRED",
        "ticker": "audcny",
        "unitPrice": null
      },
      ...
    ],
    "totalCount": 96
  }
  ```

---

### 获取兑换订单详情

`GET` /merchant/order/{orderId}

- 头部信息 header :
  - Content-Type: application/json
  - Authorization: Bearer {accessToken}
- 路径参数 path parameters

  - orderId: 兑换订单 ID

- Sample request
  ```
  https://dev-api.sdce.com.au/merchant/order/5e32538c3ce49593167fc4fd
  ```
- Sample response
  ```json
  {
    "amount": null,
    "counterPartyRequestId": "sdce-bolt",
    "createdAt": "2020-01-30T03:54:52.838Z",
    "currencyQuote": {
      "amount": {
        "amount": "1000.000000",
        "currency": "cny"
      },
      "buyUnitPrice": {
        "currency": "cny",
        "price": 5,
        "updatedAt": "2019-11-14T00:26:49.130Z"
      },
      "fee": {
        "currency": "aud",
        "value": "25"
      },
      "quantity": {
        "currency": "aud",
        "quantity": "175.000000"
      },
      "sellUnitPrice": {
        "currency": "aud",
        "price": 4,
        "updatedAt": "2019-11-14T00:26:46.196Z"
      }
    },
    "executedAt": "2020-01-30T04:19:09.134Z",
    "extraInfo": "{\"raw\":\"\"}",
    "id": "5e32538c3ce49593167fc4fd",
    "mchMargin": {
      "amount": 0,
      "currency": ""
    },
    "memo": "318027",
    "merchantId": null,
    "ownerWalletUID": 120,
    "payFundDetail": "{\"label\":\"CNY账户\",\"name\":\"张翔\",\"bankName\":\"大连银行\",\"bankDetail\":\"大连银行香一街支行\",\"bankNo\":\"12345678901234\"}",
    "quantity": null,
    "receiveFundDetail": "{\"area\":\"us/sgp/hk/ca\",\"label\":\"新加坡账户\",\"accountName\":\"xiaoman\",\"bankName\":\"HSBC\",\"accountNumber\":\"1234556\",\"street\":\"16 Huqiao Road\",\"city\":\"Hanzhong\",\"state\":\"SHAANXI SHENG\",\"postcode\":\"723000\",\"swift\":\"BA432TC\"}",
    "side": "BUY",
    "status": "OPEN",
    "ticker": "audcny",
    "unitPrice": null
  }
  ```

---

### 确认兑换订单收付款信息

`PUT` /merchant/order/{orderId}

- Description: 选择并填写收付款账户信息
- 头部信息 header :
  - Content-Type: application/json
  - Authorization: Bearer {accessToken}
- 路径参数 path parameters
  - orderId: 兑换订单 ID
- Body:

  - orderId: 兑换订单 ID
  - status: 'OPEN'
  - payFundDetail: 选择的支付信息，以 json object string 形式
    - 当支付币种非 CNY 时为"{}" (直接使用系统钱包余额)
  - receiveFundDetail: 选择的支付信息，以 json object string 形式

- Sample request:

  ```json
  https://dev-api.sdce.com.au/merchant/order/5e32538c3ce49593167fc4fd

  {
    "orderId": "5e32538c3ce49593167fc4fd",
    "status": "OPEN",
    "payFundDetail": "{\"label\":\"CNY账户\",\"name\":\"张翔\",\"bankName\":\"大连银行\",\"bankDetail\":\"大连银行香一街支行\",\"bankNo\":\"12345678901234\"}",
    "receiveFundDetail": "{\"area\":\"us/sgp/hk/ca\",\"label\":\"新加坡账户\",\"accountName\":\"xiaoman\",\"bankName\":\"HSBC\",\"accountNumber\":\"1234556\",\"street\":\"16 Huqiao Road\",\"city\":\"Hanzhong\",\"state\":\"SHAANXI SHENG\",\"postcode\":\"723000\",\"swift\":\"BA432TC\"}"
  }
  ```

- Sample response:
  ```json
  {
    "order": {
      "amount": null,
      "counterPartyRequestId": "sdce-bolt",
      "createdAt": "2020-01-30T03:54:52.838Z",
      "currencyQuote": {
        "amount": {
          "amount": "1000.000000",
          "currency": "cny"
        },
        "buyUnitPrice": {
          "currency": "cny",
          "price": 5,
          "updatedAt": "2019-11-14T00:26:49.130Z"
        },
        "fee": {
          "currency": "aud",
          "value": "25"
        },
        "quantity": {
          "currency": "aud",
          "quantity": "175.000000"
        },
        "sellUnitPrice": {
          "currency": "aud",
          "price": 4,
          "updatedAt": "2019-11-14T00:26:46.196Z"
        }
      },
      "executedAt": "1970-01-01T00:00:00.000Z",
      "extraInfo": "{\"raw\":\"\"}",
      "id": "5e32538c3ce49593167fc4fd",
      "mchMargin": {
        "amount": 0,
        "currency": ""
      },
      "memo": "654410",
      "merchantId": null,
      "ownerWalletUID": 120,
      "payFundDetail": "{\"label\":\"CNY账户\",\"name\":\"张翔\",\"bankName\":\"大连银行\",\"bankDetail\":\"大连银行香一街支行\",\"bankNo\":\"12345678901234\"}",
      "quantity": null,
      "receiveFundDetail": "{\"area\":\"us/sgp/hk/ca\",\"label\":\"新加坡账户\",\"accountName\":\"xiaoman\",\"bankName\":\"HSBC\",\"accountNumber\":\"1234556\",\"street\":\"16 Huqiao Road\",\"city\":\"Hanzhong\",\"state\":\"SHAANXI SHENG\",\"postcode\":\"723000\",\"swift\":\"BA432TC\"}",
      "side": "BUY",
      "status": "OPEN",
      "ticker": "audcny",
      "unitPrice": null
    }
  }
  ```

---

### 上传支付截图并确认

`PUT` /merchant/order/{orderId}

- Description: 当支付币种为 CNY 时，需上传场外支付证明并确认提交
- 头部信息 header :
  - Content-Type: application/json
  - Authorization: Bearer {accessToken}
- 路径参数 path parameters
  - orderId: 兑换订单 ID
- Body:

  - orderId: 兑换订单 ID
  - status: 'REVIEW'
  - screenShot: 上传文件地址，参见[上传文件](#上传文件)

- Sample request:

  ```json
  https://dev-api.sdce.com.au/merchant/order/5e32538c3ce49593167fc4fd

  {
    "orderId": "5e32538c3ce49593167fc4fd",
    "status": "REVIEW",
    "screenShot": "/bolt/1e0a7a0d-c532-462c-a0e0-34200834bd3c_wechatpay.png"
  }
  ```

- Sample response:
  ```json
  {
    "order": {
      "amount": null,
      "counterPartyRequestId": "sdce-bolt",
      "createdAt": "2020-01-30T03:54:52.838Z",
      "currencyQuote": {
        "amount": {
          "amount": "1000.000000",
          "currency": "cny"
        },
        "buyUnitPrice": {
          "currency": "cny",
          "price": 5,
          "updatedAt": "2019-11-14T00:26:49.130Z"
        },
        "fee": {
          "currency": "aud",
          "value": "25"
        },
        "quantity": {
          "currency": "aud",
          "quantity": "175.000000"
        },
        "sellUnitPrice": {
          "currency": "aud",
          "price": 4,
          "updatedAt": "2019-11-14T00:26:46.196Z"
        }
      },
      "executedAt": "2020-01-30T04:19:09.134Z",
      "extraInfo": "{\"raw\":\"\"}",
      "id": "5e32538c3ce49593167fc4fd",
      "mchMargin": {
        "amount": 0,
        "currency": ""
      },
      "memo": "318027",
      "merchantId": null,
      "ownerWalletUID": 120,
      "payFundDetail": "{\"label\":\"CNY账户\",\"name\":\"张翔\",\"bankName\":\"大连银行\",\"bankDetail\":\"大连银行香一街支行\",\"bankNo\":\"12345678901234\"}",
      "paymentProof": "/bolt/1e0a7a0d-c532-462c-a0e0-34200834bd3c_wechatpay.png",
      "quantity": null,
      "receiveFundDetail": "{\"area\":\"us/sgp/hk/ca\",\"label\":\"新加坡账户\",\"accountName\":\"xiaoman\",\"bankName\":\"HSBC\",\"accountNumber\":\"1234556\",\"street\":\"16 Huqiao Road\",\"city\":\"Hanzhong\",\"state\":\"SHAANXI SHENG\",\"postcode\":\"723000\",\"swift\":\"BA432TC\"}",
      "side": "BUY",
      "status": "REVIEW",
      "ticker": "audcny",
      "unitPrice": null
    }
  }
  ```

---

### 验证银行卡号

`GET` https://ccdcapi.alipay.com/validateAndCacheCardInfo.json?cardNo={cardNumber}&cardBinCheck=true

- 路径参数 path parameters :
  - cardNo: 银行卡号

Sample request

```json
https://ccdcapi.alipay.com/validateAndCacheCardInfo.json?cardNo=6230580000205985174&cardBinCheck=true
```

返回数据:

```json
{
  "cardType": "DC",
  "bank": "SPABANK",
  "key": "6230580000205985174",
  "messages": [],
  "validated": true,
  "stat": "ok"
}
```

---

### 激活换汇订单

`PUT` /merchant/order/{tradeId}

- 路径参数 path parameters :
  - tradeId
- Body
  - status: 'OPEN'
  - paymentDetail

Sample request

```
https://dev-api.sdce.com.au/merchant/order/5d12c56fac2cf586c704980f

body:
{
  status: 'OPEN',
  "paymentDetail": {
    "name": "xxx",
    "bankName": "北京银行",
    "bankDetail": "海淀支行",
    "cardNumber": "6230580000205985174"
  }
}
```

返回数据:

```json
{
  "counterPartyRequestId": "sdce-bolt",
  "memo": "851641",
  "tradeId": "5d12c56fac2cf586c704980f"
}
```

---

### 获取全部交易对的 ticker 数据

GET /exchange/instrument/tickers

查询参数：

返回数据：一个数组的交易对的 ticker 数据

```json
[
    {
        "id": string // 交易对的id
        "name": string //交易对的名称， 如 ORA/ETH
        "ticker": {
            "close": number // 收盘价格
            "open": number // 开盘价格
            "high": number // 最高价
            "low": number // 最低价
            "volume": number // 交易量
            "time": datetime // ticker产生时间
        }
    }
]
```

### 查询当前用户订单列表

GET /exchange/orders

查询参数：

- instrumentId: string // 交易对的 ID
- onlyOpen: boolean// 是否只查询处于 open 状态的订单
- pageIndex： integer// 返回订单列表的页数的 index
- pageSize: integer //每页包含多少订单

返回结果：

```
{
  "result": [
    {
      "events": null, // 订单时间列表
      "filled_value": "0", // 订单已经被填充的价值
      "filled_volume": "0", // 订单已经被填充的交易量
      "id": "5caf2968ec953ecd1b93cf15", // 订单id
      "instrument": {
        "base": { // 交易对被交易货币
          "decimal": 8, // 小数精确位数
          "id": "5c947d3eda0d2c6c16ef7a68", // 货币id
          "logo": "https://icons-exchange.oss-ap-southeast-2.aliyuncs.com/icon-btc.png",
          "name": {
            "en": "BTC",
            "zh": "BTC"
          },
          "symbol": "btc", // 货币代码
          "type": "CURRENCY_BTC"  // 货币类型
        },
        "code": "btcaud", // 交易对代码
        "id": "5c947d44da0d2c6c16ef7aed", // 交易对id
        "name": "BTC AUD", //交易对名称
        "quote": { // 交易对基准货币
          "decimal": 10, // 小数精确位数
          "id": "5c947d3eda0d2c6c16ef7a67", // 交易对基准货币的id,如澳币，btc, eth
          "logo": "https://icons-exchange.oss-ap-southeast-2.aliyuncs.com/icon-aud.png",
          "name": {
            "en": "AUD"
          },
          "symbol": "aud", // 基准货币代码
          "type": "CURRENCY_FIAT" // 货币类型
        }
      },
      "owner": {
        "clientId": "5c947d3eda0d2c6c16ef7982"
      },
      "price": 6938.955229781542, //订单价格
      "side": "ASK",
      "status": "OPEN", // 订单状态
      "time": "2019-04-11T11:47:52.641Z",
      "type": "LIMIT",
      "value": "44.9196042382", // 订单价值
      "volume": "0.00647354" // 订单交易量
    }
  ],
  "totalCount": 1
}
```

### 查找公开挂单列表（不需身份验证）

GET /exchange/instrument/orders

查询参数：

- instrumentId: string // 交易对的 ID
- onlyOpen: boolean// 是否只查询处于 open 状态的订单
- pageIndex： integer// 返回订单列表的页数的 index
- pageSize: integer //每页包含多少订单
- orderSide： enum // ASK|BID

返回结果：
如上

### 查看订单详情

GET /exchange/instrument/order/{orderId}

查询参数：

- orderId： string //订单 id

返回结果参照以上订单详情

### 创建订单

POST /exchange/instrument/{instrumentId}/order

路径参数：

- instrumentId: string //交易对 id

BODY:

```
    {
        side: string //BID|ASK
        type: string // MARKET|LIMIT (目前只支持两种订单)
        price： number // 订单价格
        volume： String // 交易量， 卖单必填， 买单选填
        value： String // 交易价值， 买单必填， 卖单选填
        instrument: string // 交易对的id
    }
```

返回结果：

```
    {
        "result": string // order id
    }
```

### 取消订单

DELETE /exchange/instrument/order/{orderId}

路径参数：

- orderId: string // 订单 id

query 参数：

- reason （必填）// 订单取消原因

### 取消当前用户所有订单

DELETE /exchange/orders

query 参数：

- instrumentId(选填): string // 交易对 id
- reason （选填）// 订单取消原因

### 获取最新交易列表

GET /exchange/instrument/{instrumentId}/trade

路径参数：

- instrumentId: string //交易对 id

query 参数：

- pageIndex： integer// 返回交易列表的页数的 index
- pageSize: integer //每页包含多少交易
